package rs.etf.pmu.pocketsoccer.data.room

import android.content.Context
import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import rs.etf.pmu.pocketsoccer.data.room.dao.GameInfoDao
import rs.etf.pmu.pocketsoccer.data.room.dao.UserDao
import rs.etf.pmu.pocketsoccer.data.room.entities.GameInfo
import rs.etf.pmu.pocketsoccer.data.room.entities.User
import rs.etf.pmu.pocketsoccer.data.room.type_converters.DateConverter

@TypeConverters(DateConverter::class)
@Database(entities = [User::class, GameInfo::class], version = 1, exportSchema = false)
abstract class PocketSoccerDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao

    abstract fun gameInfoDao(): GameInfoDao

    companion object {
        private const val DATABASE_NAME = "pocket_soccer_database"

        private var INSTANCE: PocketSoccerDatabase? = null

        fun getDatabase(context: Context): PocketSoccerDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = androidx.room.Room.databaseBuilder(
                    context.applicationContext,
                    PocketSoccerDatabase::class.java,
                    DATABASE_NAME
                ).build()
                INSTANCE = instance
                instance
            }
        }
    }
}
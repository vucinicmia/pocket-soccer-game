package rs.etf.pmu.pocketsoccer.ui.stateholders

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import rs.etf.pmu.pocketsoccer.utils.FlagsResource

data class EnterNamesScreenUiState(
    val playerName1: String = "",
    val playerName2: String = ""
)

data class ChoosePlayersScreenUiState(
    val playerFlagIndex1: Int = 0,
    val playerFlagIndex2: Int = 0
)

class StartGameActivityViewModel : ViewModel() {

    private val _uiStateEnterNamesScreen = MutableStateFlow(EnterNamesScreenUiState())
    val uiStateEnterNamesScreen = _uiStateEnterNamesScreen.asStateFlow()

    private val _uiStateChoosePlayersScreen = MutableStateFlow(ChoosePlayersScreenUiState())
    val uiStateChoosePlayersScreen = _uiStateChoosePlayersScreen.asStateFlow()

    fun updatePlayerName1(name: String) {
        _uiStateEnterNamesScreen.update { it.copy(playerName1 = name) }
    }

    fun updatePlayerName2(name: String) {
        _uiStateEnterNamesScreen.update { it.copy(playerName2 = name) }
    }

    fun checkIfNamesAreEntered(): Boolean {
        return uiStateEnterNamesScreen.value.playerName1.isNotEmpty() &&
                uiStateEnterNamesScreen.value.playerName2.isNotEmpty()
    }

    fun arrowUpPlayer1() {
        _uiStateChoosePlayersScreen.update {
            it.copy(
                playerFlagIndex1 = (it.playerFlagIndex1 + 1) % FlagsResource.allFlags.size
            )
        }
    }

    fun arrowDownPlayer1() {
        _uiStateChoosePlayersScreen.update {
            it.copy(
                playerFlagIndex1 = (it.playerFlagIndex1 - 1 + FlagsResource.allFlags.size) % FlagsResource.allFlags.size
            )
        }
    }

    fun arrowUpPlayer2() {
        _uiStateChoosePlayersScreen.update {
            it.copy(
                playerFlagIndex2 = (it.playerFlagIndex2 + 1) % FlagsResource.allFlags.size
            )
        }
    }

    fun arrowDownPlayer2() {
        _uiStateChoosePlayersScreen.update {
            it.copy(
                playerFlagIndex2 = (it.playerFlagIndex2 - 1 + FlagsResource.allFlags.size) % FlagsResource.allFlags.size
            )
        }
    }
}
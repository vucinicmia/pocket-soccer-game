package rs.etf.pmu.pocketsoccer.ui.elements.composables.game

import android.content.Context
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.graphics.ClipOp
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.drawscope.clipPath
import androidx.compose.ui.res.imageResource
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.IntSize
import rs.etf.pmu.pocketsoccer.game.utils.Coordinates

fun DrawScope.drawCircleImage(
    context : Context,
    imageId: Int,
    radius: Float,
    centerCoordinates: Coordinates,
    darkened: Boolean = false,
    selected: Boolean = false,
    drawBorders: Boolean = false
) {
    val height = radius * 2
    val width = radius * 2

    val x = centerCoordinates.x - radius
    val y = centerCoordinates.y - radius

    val borderStrokeWidth = if (selected) 10f else 3f

    if (drawBorders) {
        drawCircle(
            color = Color.White,
            radius = radius + borderStrokeWidth,
            center = centerCoordinates.toOffset()
        )
    }

    val clipPath = Path().apply {
        addOval(
            Rect(
                left = x,
                top = y,
                right = x + width,
                bottom = y + height
            )
        )
    }

    clipPath(clipPath, ClipOp.Intersect) {
        val image = ImageBitmap.imageResource(res = context.resources, id = imageId)
        drawImage(
            image = image,
            dstOffset = IntOffset(x.toInt(), y.toInt()),
            dstSize = IntSize(width.toInt(), height.toInt())
        )
    }

    if (darkened) {
        drawCircle(
            color = Color.Black.copy(alpha = 0.5f),
            radius = radius + borderStrokeWidth,
            center = centerCoordinates.toOffset()
        )
    }
}
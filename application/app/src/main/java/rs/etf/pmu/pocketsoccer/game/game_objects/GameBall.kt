package rs.etf.pmu.pocketsoccer.game.game_objects

import rs.etf.pmu.pocketsoccer.game.utils.AccelerationVector
import rs.etf.pmu.pocketsoccer.game.utils.Coordinates
import rs.etf.pmu.pocketsoccer.game.utils.VelocityVector
import kotlin.math.abs

open class GameBall (
    val center: Coordinates,
    val startCoordinates: Coordinates,
    val r: Float,
    val mass: Float,
    val velocityVector: VelocityVector,
    val accelerationVector: AccelerationVector = AccelerationVector(0f, 0f),
    val id: Int = ++_id
) {
    companion object {
        private var _id: Int = 0
    }

    fun doesItOverlap(ball: GameBall): Boolean {
        return abs((center.x - ball.center.x) * (center.x - ball.center.x) + (center.y - ball.center.y) * (center.y - ball.center.y)) < (r + ball.r) * (r + ball.r)
    }

    fun isPointInIt(point_x: Float, point_y: Float): Boolean {
        return abs((center.x - point_x) * (center.x - point_x) + (center.y - point_y) * (center.y- point_y)) < (r * r)
    }

    fun resetState() {
        accelerationVector.ax = 0f
        accelerationVector.ay = 0f
        velocityVector.vx = 0f
        velocityVector.vy = 0f
        center.x = startCoordinates.x
        center.y = startCoordinates.y
    }
}
package rs.etf.pmu.pocketsoccer.game.game_objects

import rs.etf.pmu.pocketsoccer.game.Player
import rs.etf.pmu.pocketsoccer.game.utils.Coordinates
import rs.etf.pmu.pocketsoccer.utils.WindowUtils

class Goal(
    val goalBoundaries: List<Line>,
    val player: Player
) {
    fun isBallInGoal(ball: GameBall): Boolean {
        val lineUp = goalBoundaries[0]
        val lineDown = goalBoundaries[1]

        if (
            ball.center.x + ball.r <= lineUp.endCoordinates.x &&
            ball.center.x - ball.r >= lineUp.startCoordinates.x &&
            ball.center.y - ball.r >= lineUp.endCoordinates.y &&
            ball.center.y + ball.r <= lineDown.endCoordinates.y
        ) {
            return true
        }

        return false
    }

    companion object {

        private val SCREEN_WIDTH = WindowUtils.WIDTH.toFloat()
        private val SCREEN_HEIGHT = WindowUtils.HEIGHT.toFloat()

        private const val GOAL_LINE_RADIUS = 10f

        /*
            0
            *****************************************
            *       1/8 W
            *        |
    1/4 H   **********
            *
            *
            *
            *       1/8 W
            *        |
    3/4 H   **********
            *
            *
            *****************************************
        */

        val GOAL_LEFT = Goal(
            goalBoundaries = listOf(
                Line(
                    startCoordinates = Coordinates(0f, 1f / 4f * SCREEN_HEIGHT),
                    endCoordinates = Coordinates(1f / 8f * SCREEN_WIDTH, 1f / 4f * SCREEN_HEIGHT),
                    lineRadius = GOAL_LINE_RADIUS
                ),
                Line(
                    startCoordinates = Coordinates(0f, 3f / 4f * SCREEN_HEIGHT),
                    endCoordinates = Coordinates(1f / 8f * SCREEN_WIDTH, 3f / 4f * SCREEN_HEIGHT),
                    lineRadius = GOAL_LINE_RADIUS
                )
            ),
            player = Player.PLAYER_1
        )

        /*
            0
            *****************************************
                                          7/8 W     *
                                           |        *
                                           ********** - 1/4 H
                                                    *
                                                    *
                                                    *
                                          7/8 W     *
                                           |        *
                                           ********** - 3/4 H
                                                    *
                                                    *
            *****************************************
        */

        val GOAL_RIGHT = Goal(
            goalBoundaries = listOf(
                Line(
                    startCoordinates = Coordinates(7f / 8f * SCREEN_WIDTH, 1f / 4f * SCREEN_HEIGHT),
                    endCoordinates = Coordinates(SCREEN_WIDTH, 1f / 4f * SCREEN_HEIGHT),
                    lineRadius = GOAL_LINE_RADIUS
                ),
                Line(
                    startCoordinates = Coordinates(7f / 8f * SCREEN_WIDTH, 3f / 4f * SCREEN_HEIGHT),
                    endCoordinates = Coordinates(SCREEN_WIDTH, 3f / 4f * SCREEN_HEIGHT),
                    lineRadius = GOAL_LINE_RADIUS
                )
            ),
            player = Player.PLAYER_2
        )

        val GOALS = listOf(GOAL_LEFT, GOAL_RIGHT)
    }
}
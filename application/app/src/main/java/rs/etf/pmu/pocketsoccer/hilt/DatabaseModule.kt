package rs.etf.pmu.pocketsoccer.hilt

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton
import rs.etf.pmu.pocketsoccer.data.room.PocketSoccerDatabase

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext appContext: Context): PocketSoccerDatabase {
        return PocketSoccerDatabase.getDatabase(appContext)
    }

    @Provides
    @Singleton
    fun provideUserDao(database: PocketSoccerDatabase) = database.userDao()

    @Provides
    @Singleton
    fun provideGameInfoDao(database: PocketSoccerDatabase) = database.gameInfoDao()
}
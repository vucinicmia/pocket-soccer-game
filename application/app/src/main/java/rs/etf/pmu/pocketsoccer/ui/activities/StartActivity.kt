package rs.etf.pmu.pocketsoccer.ui.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import rs.etf.pmu.pocketsoccer.ui.elements.screens.LogInScreen
import rs.etf.pmu.pocketsoccer.ui.elements.screens.OnboardScreen
import rs.etf.pmu.pocketsoccer.ui.elements.screens.RegisterScreen
import rs.etf.pmu.pocketsoccer.ui.navigation.LogInScreenDestination
import rs.etf.pmu.pocketsoccer.ui.navigation.OnboardScreenDestination
import rs.etf.pmu.pocketsoccer.ui.navigation.RegisterScreenDestination
import rs.etf.pmu.pocketsoccer.ui.stateholders.StartActivityViewModel
import rs.etf.pmu.pocketsoccer.ui.theme.PocketSoccerTheme
import rs.etf.pmu.pocketsoccer.utils.CameraUtils
import rs.etf.pmu.pocketsoccer.utils.SharedPreferencesKeys
import rs.etf.pmu.pocketsoccer.utils.SharedPreferencesUtils
import rs.etf.pmu.pocketsoccer.utils.WindowUtils

@AndroidEntryPoint
class StartActivity : ComponentActivity() {

    private lateinit var viewModelStartActivity: StartActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            PocketSoccerTheme {
                val startGameActivity = {
                    val intent = Intent(this@StartActivity, StartGameActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    finish()
                }

                val rememberLogin = SharedPreferencesUtils.getBoolean(
                    this@StartActivity,
                    SharedPreferencesKeys.REMEMBER_LOG_IN,
                    false
                )

                if (rememberLogin) {
                    startGameActivity()
                } else {
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = MaterialTheme.colorScheme.background
                    ) {
                        viewModelStartActivity = viewModel()

                        StartActivityComposable(
                            viewModelStartActivity = viewModelStartActivity,
                            takePhotoCallback = { takePhoto.launch(null) },
                            startGameActivity = startGameActivity
                        )
                    }
                }
            }
        }

        WindowUtils.initializeWidthAndHeight(this)
        WindowUtils.hideSystemUI(this)
    }

    private val takePhoto =
        registerForActivityResult(ActivityResultContracts.TakePicturePreview()) { image ->
            val fileName = "${viewModelStartActivity.uiStateRegisterScreen.value.username}.jpg"
            val uriDeferred = lifecycleScope.async(Dispatchers.IO) {
                CameraUtils.savePhotoToFile(
                    this@StartActivity,
                    image,
                    fileName
                )
            }
            lifecycleScope.launch {
                val uri = uriDeferred.await()
                viewModelStartActivity.updatePhotoPath(uri.toString())
            }
        }
}

@Composable
fun StartActivityComposable(
    viewModelStartActivity: StartActivityViewModel,
    takePhotoCallback: () -> Unit,
    startGameActivity: () -> Unit
) {
    val navController = rememberNavController()

    val uiState by viewModelStartActivity.uiStateLoginScreen.collectAsState()

    val context = LocalContext.current

    val onSuccessfulLogInCallback = {
        SharedPreferencesUtils.putBoolean(
            context,
            SharedPreferencesKeys.REMEMBER_LOG_IN,
            uiState.rememberLogin
        )
        startGameActivity()
    }

    NavHost(
        navController = navController,
        startDestination = OnboardScreenDestination.route
    ) {
        composable(route = OnboardScreenDestination.route) {
            OnboardScreen(
                onButtonLogInClickedCallback = {
                    navController.navigate(LogInScreenDestination.route)
                }, onButtonRegisterClickedCallback = {
                    navController.navigate(RegisterScreenDestination.route)
                }
            )
        }

        composable(route = LogInScreenDestination.route) {
            LogInScreen(
                viewModel = viewModelStartActivity,
                onSuccessfulLogInCallback = onSuccessfulLogInCallback,
                onErrorCallback = { message ->
                    Toast.makeText(
                        context,
                        message,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            )
        }

        composable(route = RegisterScreenDestination.route) {
            RegisterScreen(
                viewModel = viewModelStartActivity,
                takePhotoCallback = takePhotoCallback,
                onSuccessfulRegistrationCallback = startGameActivity,
                onErrorCallback = { message ->
                    Toast.makeText(
                        context,
                        message,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            )
        }
    }
}
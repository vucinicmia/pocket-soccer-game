package rs.etf.pmu.pocketsoccer.ui.elements.screens.settings

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import rs.etf.pmu.pocketsoccer.game.settings.GameSpeed
import rs.etf.pmu.pocketsoccer.ui.applyIf
import rs.etf.pmu.pocketsoccer.ui.elements.composables.StadiumBackground

@Composable
fun GameSpeedSettingsScreen(
    currentGameSpeed: GameSpeed,
    onChangedCallback: (GameSpeed) -> Unit
) {
    var selectedSpeed by rememberSaveable {
        mutableStateOf(currentGameSpeed)
    }

    StadiumBackground {
        Text(
            text = "Choose game\nspeed",
            modifier = Modifier
                .fillMaxWidth(),
            style = MaterialTheme.typography.titleMedium,
            textAlign = TextAlign.Center,
        )

        Row(
            modifier = Modifier
                .fillMaxSize()
                .padding(bottom = 30.dp),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Column(
                modifier = Modifier.weight(1f),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                val borderWidth = if (selectedSpeed == GameSpeed.SLOW) 2.dp else 1.dp
                Text(
                    text = "SLOW",
                    style = MaterialTheme.typography.displayLarge,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .border(borderWidth, Color.LightGray, CircleShape)
                        .applyIf(selectedSpeed == GameSpeed.SLOW) {
                            background(Color.LightGray.copy(alpha = 0.3f), CircleShape)
                        }
                        .padding(10.dp)
                        .clickable {
                            selectedSpeed = GameSpeed.SLOW
                            onChangedCallback(selectedSpeed)
                        },
                )
            }

            Column(
                modifier = Modifier.weight(1f),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                val borderWidth = if (selectedSpeed == GameSpeed.NORMAL) 2.dp else 1.dp
                Text(
                    text = "NORMAL",
                    style = MaterialTheme.typography.displayLarge,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .border(borderWidth, Color.LightGray, CircleShape)
                        .applyIf(selectedSpeed == GameSpeed.NORMAL) {
                            background(Color.LightGray.copy(alpha = 0.3f), CircleShape)
                        }
                        .padding(10.dp)
                        .clickable {
                            selectedSpeed = GameSpeed.NORMAL
                            onChangedCallback(selectedSpeed)
                        },
                )
            }

            Column(
                modifier = Modifier.weight(1f),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                val borderWidth = if (selectedSpeed == GameSpeed.FAST) 2.dp else 1.dp
                Text(
                    text = "FAST",
                    style = MaterialTheme.typography.displayLarge,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .border(borderWidth, Color.LightGray, CircleShape)
                        .applyIf(selectedSpeed == GameSpeed.FAST) {
                            background(Color.LightGray.copy(alpha = 0.3f), CircleShape)
                        }
                        .padding(10.dp)
                        .clickable {
                            selectedSpeed = GameSpeed.FAST
                            onChangedCallback(selectedSpeed)
                        },
                )
            }
        }
    }
}
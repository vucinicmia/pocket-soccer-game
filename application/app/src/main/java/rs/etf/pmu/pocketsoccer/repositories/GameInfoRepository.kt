package rs.etf.pmu.pocketsoccer.repositories

import rs.etf.pmu.pocketsoccer.data.room.dao.GameInfoDao
import rs.etf.pmu.pocketsoccer.data.room.entities.GameInfo
import javax.inject.Inject

class GameInfoRepository @Inject constructor(private val gameInfoDao: GameInfoDao) {

    suspend fun getAllGames(): List<GameInfo> {
        return gameInfoDao.getAllGames()
    }

    suspend fun getGameInfoById(id: Int): GameInfo {
        return gameInfoDao.getGameInfoById(id)
    }

    suspend fun insertGameInfo(gameInfo: GameInfo) {
        gameInfoDao.insertGameInfo(gameInfo)
    }

    suspend fun deleteAllGames() {
        gameInfoDao.deleteAllGames()
    }

    suspend fun getAllGamesForPlayers(playerName1: String, playerName2: String): List<GameInfo> {
        return gameInfoDao.getAllGamesForPlayers(playerName1, playerName2)
    }

    suspend fun deleteAllGamesForPlayers(playerName1: String, playerName2: String) {
        gameInfoDao.deleteAllGamesForPlayers(playerName1, playerName2)
    }
}
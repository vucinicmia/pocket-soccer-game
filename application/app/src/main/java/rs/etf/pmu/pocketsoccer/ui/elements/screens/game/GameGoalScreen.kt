package rs.etf.pmu.pocketsoccer.ui.elements.screens.game

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import rs.etf.pmu.pocketsoccer.game.settings.EndGameCondition
import rs.etf.pmu.pocketsoccer.ui.elements.composables.game.DarkenedLayer

@Composable
fun GameGoalScreen(
    playerResult1: Int,
    playerResult2: Int,
    endGameCondition: EndGameCondition,
    timeElapsed: Long? = null,
    timeLimit: Long? = null,
    onGoalButtonClickCallback: () -> Unit = {},
) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(start = 20.dp, top = 10.dp)
    ) {
        Text(
            text = "$playerResult1 : $playerResult2",
            modifier = Modifier.align(Alignment.TopCenter),
            style = MaterialTheme.typography.displayLarge
        )

        if (endGameCondition == EndGameCondition.TIMEOUT) {
            timeLimit?.let { limit ->
                timeElapsed?.let {
                    Text(
                        text = "${it / 1000}:${limit / 1000}",
                        fontSize = 50.sp,
                        modifier = Modifier.align(Alignment.TopStart)
                    )
                }
            }
        }
    }

    Box(
        modifier = Modifier
            .fillMaxSize()
    ) {
        DarkenedLayer()

        Text(
            text = "GOAL",
            modifier = Modifier
                .align(Alignment.Center)
                .clickable(onClick = onGoalButtonClickCallback),
            style = MaterialTheme.typography.displayLarge
        )
    }
}
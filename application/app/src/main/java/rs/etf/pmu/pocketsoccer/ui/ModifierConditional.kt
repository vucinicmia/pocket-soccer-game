package rs.etf.pmu.pocketsoccer.ui

import androidx.compose.ui.Modifier

/**
 * Applies [modifier] if [condition] is true.
 * Source: https://stackoverflow.com/a/72554087
 * @param condition if true, [modifier] will be applied
 */
fun Modifier.applyIf(condition: Boolean, modifier: Modifier.() -> Modifier): Modifier {
    return if (condition) {
        then(modifier(Modifier))
    } else {
        this
    }
}
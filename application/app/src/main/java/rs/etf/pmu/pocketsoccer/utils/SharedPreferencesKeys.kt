package rs.etf.pmu.pocketsoccer.utils

object SharedPreferencesKeys {

    const val REMEMBER_LOG_IN = "remember_log_in"

    const val SAVED_GAME = "saved_game"

    // settings
    const val SETTINGS_END_GAME_CONDITION = "settings_end_game_condition"
    const val SETTINGS_GAME_SPEED = "settings_game_speed"
    const val SETTINGS_FIELD = "settings_field"
    const val SETTINGS_NUMBER_OF_GOALS = "settings_number_of_goals"
    const val SETTINGS_TIMEOUT = "settings_timeout"

}
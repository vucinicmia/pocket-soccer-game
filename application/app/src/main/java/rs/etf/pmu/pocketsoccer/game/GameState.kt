package rs.etf.pmu.pocketsoccer.game

import rs.etf.pmu.pocketsoccer.game.game_objects.FootballBall
import rs.etf.pmu.pocketsoccer.game.game_objects.GameBall
import rs.etf.pmu.pocketsoccer.game.game_objects.PlayerBall
import rs.etf.pmu.pocketsoccer.game.settings.EndGameCondition
import rs.etf.pmu.pocketsoccer.game.settings.GameMode
import rs.etf.pmu.pocketsoccer.game.settings.GameSpeed
import rs.etf.pmu.pocketsoccer.game.utils.AccelerationVector
import rs.etf.pmu.pocketsoccer.game.utils.Coordinates
import rs.etf.pmu.pocketsoccer.game.utils.VelocityVector
import rs.etf.pmu.pocketsoccer.utils.WindowUtils
import java.util.EnumMap
import kotlin.random.Random

class GameState(
    var gameMode: GameMode,
    var endGameCondition: EndGameCondition,
    var numberOfGoalsToWin: Int,
    var timeElapsed: Long,
    var timeLimit: Long,
    var gameSpeed: GameSpeed,
    var fieldImageId: Int,
    var teamPlayers1: MutableList<PlayerBall>,
    var teamPlayers2: MutableList<PlayerBall>,
    var ball: FootballBall,
    var gameBalls: MutableList<GameBall>,
    var currentGameState: GameStates,
    var playerOnTurn: Player,
    val result: EnumMap<Player, Int>,
    var moveTime: Long,
    var playerName1: String,
    var playerName2: String,
) {

    fun switchTurn() {
        playerOnTurn = if (playerOnTurn == Player.PLAYER_1)
            Player.PLAYER_2
        else
            Player.PLAYER_1
        moveTime = 0
    }

    fun changeGameState(newGameState: GameStates) {
        val oldState = currentGameState

        if (oldState == GameStates.GOAL) {
            when (endGameCondition) {
                EndGameCondition.NUMBER_OF_GOALS -> {
                    val numberOfGoals = result[Player.PLAYER_1]!! + result[Player.PLAYER_2]!!
                    if (numberOfGoals == numberOfGoalsToWin)
                        currentGameState = GameStates.FINISHED
                }

                EndGameCondition.TIMEOUT -> {
                    if (timeElapsed >= timeLimit) {
                        currentGameState = GameStates.FINISHED
                    }
                }
            }
        }

        if (newGameState == GameStates.RUNNING) {
            if (oldState == GameStates.GOAL) {
                resetGameState()
            }
        }

        currentGameState = newGameState
    }

    fun findAndMoveClosestPlayerToTheBall() {
        var minDistance = Float.MAX_VALUE
        var closestPlayer: PlayerBall? = null

        teamPlayers1.forEach { player ->
            val distance = player.center.distanceTo(ball.center)
            if (distance < minDistance) {
                minDistance = distance
                closestPlayer = player
            }
        }

        closestPlayer?.run {
            val force = Random.nextInt(1, 10).toFloat()
            velocityVector.vx = force * (ball.center.x - center.x)
            velocityVector.vy = force * (ball.center.y - center.y)
        }
    }

    private fun resetGameState() {
        playerOnTurn = Player.PLAYER_1

        gameBalls.forEach { ball ->
            ball.resetState()
        }

        moveTime = 0
    }

    companion object {
        fun createGameState(
            gameMode: GameMode,
            playerName1: String,
            playerName2: String,
            playerFlag1: Int,
            playerFlag2: Int,
            endGameCondition: EndGameCondition,
            numberOfGoals: Int,
            timeout: Long,
            fieldImageId: Int,
            gameSpeed: GameSpeed
        ): GameState {

            val footballRadius = PlayerBall.PLAYER_RADIUS / 2f

            val screenWidth = WindowUtils.WIDTH
            val screenHeight = WindowUtils.HEIGHT

            val playerCoordinates = listOf(
                Coordinates(screenWidth / 4f, screenHeight / 4f),
                Coordinates(screenWidth / 4f, screenHeight - screenHeight / 4f),
                Coordinates(3f * screenWidth / 8f, screenHeight / 2f)
            )

            val teamPlayers1 = mutableListOf<PlayerBall>()
            val teamPlayers2 = mutableListOf<PlayerBall>()
            val gameBalls = mutableListOf<GameBall>()

            val playerOnTurn = Player.PLAYER_1

            playerCoordinates.forEach { c ->

                val playerBall1 = PlayerBall(
                    centerCoordinates = c,
                    startCoordinates = c.copy(),
                    r = PlayerBall.PLAYER_RADIUS,
                    mass = 25f * PlayerBall.PLAYER_RADIUS,
                    velocityVector = VelocityVector(0f, 0f),
                    accelerationVector = AccelerationVector(0f, 0f),
                    player = Player.PLAYER_1,
                    flagImage = playerFlag1
                )

                teamPlayers1.add(playerBall1)

                val playerBall2 = PlayerBall(
                    centerCoordinates = Coordinates(screenWidth - c.x, c.y),
                    startCoordinates = Coordinates(screenWidth - c.x, c.y),
                    r = PlayerBall.PLAYER_RADIUS,
                    mass = 25f * PlayerBall.PLAYER_RADIUS,
                    velocityVector = VelocityVector(0f, 0f),
                    accelerationVector = AccelerationVector(0f, 0f),
                    player = Player.PLAYER_2,
                    flagImage = playerFlag2
                )

                teamPlayers2.add(playerBall2)

                gameBalls.add(playerBall1)
                gameBalls.add(playerBall2)
            }

            val ball = FootballBall(
                centerCoordinates = Coordinates(screenWidth / 2f, screenHeight / 2f),
                startCoordinates = Coordinates(screenWidth / 2f, screenHeight / 2f),
                r = footballRadius,
                mass = 15f * footballRadius,
                velocityVector = VelocityVector(0f, 0f),
                accelerationVector = AccelerationVector(0f, 0f)
            )

            gameBalls.add(ball)

            return GameState(
                gameMode = gameMode,
                endGameCondition = endGameCondition,
                numberOfGoalsToWin = numberOfGoals,
                timeElapsed = 0,
                timeLimit = timeout,
                gameSpeed = gameSpeed,
                fieldImageId = fieldImageId,
                teamPlayers1 = teamPlayers1,
                teamPlayers2 = teamPlayers2,
                ball = ball,
                gameBalls = gameBalls,
                currentGameState = GameStates.NOT_STARTED,
                playerOnTurn = playerOnTurn,
                result = EnumMap(mapOf(Player.PLAYER_1 to 0, Player.PLAYER_2 to 0)),
                moveTime = 0,
                playerName1 = playerName1,
                playerName2 = playerName2
            )
        }
    }
}
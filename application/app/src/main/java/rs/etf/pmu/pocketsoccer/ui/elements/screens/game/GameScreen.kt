package rs.etf.pmu.pocketsoccer.ui.elements.screens.game

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalContext
import rs.etf.pmu.pocketsoccer.R
import rs.etf.pmu.pocketsoccer.game.Game
import rs.etf.pmu.pocketsoccer.game.GameStates
import rs.etf.pmu.pocketsoccer.game.Player
import rs.etf.pmu.pocketsoccer.game.game_objects.Field
import rs.etf.pmu.pocketsoccer.game.game_objects.FootballBall
import rs.etf.pmu.pocketsoccer.game.game_objects.Goal
import rs.etf.pmu.pocketsoccer.game.game_objects.PlayerBall
import rs.etf.pmu.pocketsoccer.ui.elements.composables.game.ImageBackground
import rs.etf.pmu.pocketsoccer.ui.elements.composables.game.drawCircleImage
import rs.etf.pmu.pocketsoccer.ui.stateholders.GameViewModel

@Composable
fun GameScreen(
    viewModel: GameViewModel,
    onSaveAndExitGameButtonClickCallback: () -> Unit = {},
    onExitGameButtonClickCallback: () -> Unit = {},
    onFinishedGameButtonClickCallback: () -> Unit = {}
) {
    val uiState by viewModel.uiState.collectAsState()

    Box(
        modifier = Modifier
            .fillMaxSize()
    ) {

        val game = uiState.game.gameState

        val context = LocalContext.current

        synchronized(Game) {

            ImageBackground(imageId = game.fieldImageId) {
                Canvas(
                    modifier = Modifier
                        .fillMaxSize()
                        .pointerInput(Unit) {
                            detectDragGestures(
                                onDragStart = { offset ->
                                    viewModel.setSelectedPlayer(offset.x, offset.y)
                                    viewModel.startDrag();
                                },
                                onDragEnd = {
                                    viewModel.onEndDrag()
                                },
                                onDrag = { change, dragAmount ->
                                    change.consume()
                                    viewModel.onDrag(dragAmount.x, dragAmount.y)
                                }
                            )
                        }
                ) {

                    // Draw ball and players
                    game.gameBalls.forEach { ball ->

                        if (ball is FootballBall) {
                            drawCircleImage(
                                context = context,
                                imageId = R.drawable.ball,
                                radius = ball.r,
                                centerCoordinates = ball.center
                            )
                        } else {
                            val selected = Game.selectedPlayer == ball
                            val player: PlayerBall = ball as PlayerBall
                            val darkened = game.playerOnTurn != player.player

                            drawCircleImage(
                                context = context,
                                imageId = player.flagImage,
                                radius = ball.r,
                                centerCoordinates = ball.center,
                                darkened = darkened,
                                selected = selected,
                                drawBorders = true
                            )
                        }
                    }

                    // Draw field edges
                    Field.EDGES.forEach { line ->
                        with(line) {
                            drawLine(
                                start = startCoordinates.toOffset(),
                                end = endCoordinates.toOffset(),
                                color = Color.White,
                                strokeWidth = lineRadius
                            )
                        }
                    }

                    // Draw goals
                    Goal.GOALS.forEach { goal ->
                        goal.goalBoundaries.forEach { line ->
                            with(line) {
                                drawLine(
                                    start = startCoordinates.toOffset(),
                                    end = endCoordinates.toOffset(),
                                    color = Color.White,
                                    strokeWidth = lineRadius
                                )
                            }
                        }
                    }

                    // Draw dragging line
                    if (uiState.dragging) {
                        val draggingLine = uiState.draggingLine

                        with(draggingLine) {
                            drawLine(
                                start = startCoordinates.toOffset(),
                                end = endCoordinates.toOffset(),
                                color = Color.White,
                                strokeWidth = lineRadius
                            )
                        }
                    }
                }

                val playerResult1 = game.result[Player.PLAYER_1]!!
                val playerResult2 = game.result[Player.PLAYER_2]!!

                when (game.currentGameState) {
                    GameStates.NOT_STARTED -> {
                        GameStartScreen(onStartClickedCallback = viewModel::runGame)
                    }

                    GameStates.RUNNING -> {
                        GamePlayScreen(
                            playerResult1 = playerResult1,
                            playerResult2 = playerResult2,
                            endGameCondition = game.endGameCondition,
                            timeElapsed = game.timeElapsed,
                            timeLimit = game.timeLimit,
                            onPauseButtonClickCallback = viewModel::pauseGame,
                        )
                    }

                    GameStates.PAUSED -> {
                        GamePauseScreen(
                            onResumeButtonClickCallback = viewModel::runGame,
                            onSaveAndExitGameButtonClickCallback = onSaveAndExitGameButtonClickCallback,
                            onExitGameButtonClickCallback = onExitGameButtonClickCallback
                        )
                    }

                    GameStates.GOAL -> {
                        GameGoalScreen(
                            playerResult1 = playerResult1,
                            playerResult2 = playerResult2,
                            endGameCondition = game.endGameCondition,
                            timeElapsed = game.timeElapsed,
                            timeLimit = game.timeLimit,
                            onGoalButtonClickCallback = viewModel::runGame
                        )
                    }

                    GameStates.FINISHED -> {
                        GameFinishedScreen(
                            playerResult1 = playerResult1,
                            playerResult2 = playerResult2,
                            onFinishedGameButtonClickCallback = onFinishedGameButtonClickCallback,
                        )
                    }
                }
            }
        }
    }
}
package rs.etf.pmu.pocketsoccer.ui.stateholders

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import rs.etf.pmu.pocketsoccer.data.room.entities.User
import rs.etf.pmu.pocketsoccer.repositories.UserRepository
import javax.inject.Inject

data class LogInScreenUiState(
    val username: String = "",
    val password: String = "",
    val rememberLogin: Boolean = false
)

data class RegisterScreenUiState(
    val username: String = "",
    val password: String = "",
    val photoTaken: Boolean = false,
    val photoPath: String = ""
)

@HiltViewModel
class StartActivityViewModel @Inject constructor(
    private val userRepository: UserRepository
) : ViewModel() {

    private val _uiStateLoginScreen = MutableStateFlow(LogInScreenUiState())
    val uiStateLoginScreen = _uiStateLoginScreen.asStateFlow()

    private val _uiStateRegisterScreen = MutableStateFlow(RegisterScreenUiState())
    val uiStateRegisterScreen = _uiStateRegisterScreen.asStateFlow()

    fun login(onSuccessfulLogInCallback: () -> Unit, onErrorCallback: (String) -> Unit) {
        if (uiStateLoginScreen.value.username.isEmpty()) {
            onErrorCallback("Please enter your username!")
            return
        }
        if (uiStateLoginScreen.value.password.isEmpty()) {
            onErrorCallback("Please enter your password!")
            return
        }
        viewModelScope.launch(Dispatchers.IO) {
            val user = userRepository.getUser(uiStateLoginScreen.value.username)
            if (user == null) {
                withContext(Dispatchers.Main) {
                    onErrorCallback("Please check your username!")
                }
            } else if (user.password != uiStateLoginScreen.value.password) {
                withContext(Dispatchers.Main) {
                    onErrorCallback("Wrong password")
                }
            } else {
                withContext(Dispatchers.Main) {
                    onSuccessfulLogInCallback()
                }
            }
        }
    }

    fun register(onSuccessfulRegistrationCallback: () -> Unit, onErrorCallback: (String) -> Unit) {
        if (uiStateRegisterScreen.value.username.isEmpty()) {
            onErrorCallback("Please enter your username!")
            return
        }
        if (uiStateRegisterScreen.value.password.isEmpty()) {
            onErrorCallback("Please enter your password!")
            return
        }
        viewModelScope.launch(Dispatchers.IO) {
            val user = userRepository.getUser(uiStateRegisterScreen.value.username)
            if (user != null) {
                withContext(Dispatchers.Main) {
                    onErrorCallback("Username already exists!")
                }
            } else {
                userRepository.insertUser(
                    with(uiStateRegisterScreen.value) {
                        User(
                            username = username,
                            password = password,
                            photoPath = photoPath
                        )
                    }
                )
                withContext(Dispatchers.Main) {
                    onSuccessfulRegistrationCallback()
                }
            }
        }
    }

    fun takePhoto(takePhotoCallback: () -> Unit, onErrorCallback: (String) -> Unit) {
        if (uiStateRegisterScreen.value.photoTaken) {
            return
        }
        try {
            takePhotoCallback()
            _uiStateRegisterScreen.update { it.copy(photoTaken = true) }
        } catch (e: Exception) {
            onErrorCallback("Error while taking photo!")
        }
    }

    fun updateUsernameLogInScreen(username: String) {
        _uiStateLoginScreen.update { it.copy(username = username) }
    }

    fun updatePasswordLogInScreen(password: String) {
        _uiStateLoginScreen.update { it.copy(password = password) }
    }

    fun updateRememberLogin(value: Boolean) {
        _uiStateLoginScreen.update { it.copy(rememberLogin = value) }
    }

    fun updateUsernameRegisterScreen(username: String) {
        _uiStateRegisterScreen.update { it.copy(username = username) }
    }

    fun updatePasswordRegisterScreen(password: String) {
        _uiStateRegisterScreen.update { it.copy(password = password) }
    }

    fun updatePhotoPath(photoPath: String) {
        _uiStateRegisterScreen.update { it.copy(photoPath = photoPath) }
    }
}
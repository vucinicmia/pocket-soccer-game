package rs.etf.pmu.pocketsoccer.game

import rs.etf.pmu.pocketsoccer.game.game_objects.Field
import rs.etf.pmu.pocketsoccer.game.game_objects.FootballBall
import rs.etf.pmu.pocketsoccer.game.game_objects.GameBall
import rs.etf.pmu.pocketsoccer.game.game_objects.Goal
import rs.etf.pmu.pocketsoccer.game.game_objects.Line
import rs.etf.pmu.pocketsoccer.game.game_objects.PlayerBall
import rs.etf.pmu.pocketsoccer.game.settings.EndGameCondition
import rs.etf.pmu.pocketsoccer.game.settings.GameMode
import rs.etf.pmu.pocketsoccer.game.settings.GameSpeed
import rs.etf.pmu.pocketsoccer.game.utils.Coordinates
import rs.etf.pmu.pocketsoccer.game.utils.VelocityVector
import java.lang.Float.max
import kotlin.math.abs
import kotlin.math.min
import kotlin.math.sqrt

object Game {

    lateinit var gameState: GameState

    var selectedPlayer: PlayerBall? = null

    private val ALL_LINES: MutableList<Line> = mutableListOf()

    private val GOALS: List<Goal> = listOf(Goal.GOAL_LEFT, Goal.GOAL_RIGHT)

    const val INTENT_TAG_CONTINUE_GAME = "continue_game"

    init {
        // Add goals
        Goal.GOALS.forEach { goal ->
            goal.goalBoundaries.forEach { line ->
                ALL_LINES.add(line)
            }
        }

        // Add edges
        Field.EDGES.forEach { line ->
            ALL_LINES.add(line)
        }
    }

    fun setSelectedPlayer(x: Float, y: Float) {
        with(gameState) {
            if (playerOnTurn == Player.PLAYER_1) {
                teamPlayers1.find {
                    it.isPointInIt(x, y)
                }?.let {
                    selectedPlayer = it
                }
            } else {
                teamPlayers2.find {
                    it.isPointInIt(x, y)
                }?.let {
                    selectedPlayer = it
                }
            }
        }
    }

    fun unselectPlayer() {
        selectedPlayer = null
    }

    fun initGame(
        gameMode: GameMode,
        playerName1: String,
        playerName2: String,
        playerFlag1: Int,
        playerFlag2: Int,
        endGameCondition: EndGameCondition,
        numberOfGoals: Int,
        timeout: Long,
        fieldImageId: Int,
        gameSpeed: GameSpeed
    ) {
        selectedPlayer = null

        gameState = GameState.createGameState(
            gameMode,
            playerName1,
            playerName2,
            playerFlag1,
            playerFlag2,
            endGameCondition,
            numberOfGoals,
            timeout,
            fieldImageId,
            gameSpeed
        )
    }


    // Source: javidx9
    // Programming Balls #1 Circle Vs Circle Collisions C++ https://www.youtube.com/watch?v=LPzyNOHY3A4
    // Programming Balls #2 Circles V Edges Collisions C++ https://www.youtube.com/watch?v=ebq7L2Wtbl4
    fun onUpdate(
        elapsedTime: Float,
        onGoalScoredCallback: () -> Unit,
        onBallCollisionCallback: () -> Unit
    ) {
        with(gameState) {
            for (ball in gameBalls) {
                ball.accelerationVector.ax = -ball.velocityVector.vx * 0.8f
                ball.accelerationVector.ay = -ball.velocityVector.vx * 0.8f

                ball.velocityVector.vx += ball.accelerationVector.ax * elapsedTime
                ball.velocityVector.vy += ball.accelerationVector.ay * elapsedTime
                ball.center.x += ball.velocityVector.vx * elapsedTime
                ball.center.y += ball.velocityVector.vy * elapsedTime

                if (abs(ball.velocityVector.vx * ball.velocityVector.vx + ball.velocityVector.vy * ball.velocityVector.vy) < 0.01f) {
                    ball.velocityVector.vx = 0f
                    ball.velocityVector.vy = 0f
                }
            }

            val collidingBallPairs: MutableList<Pair<GameBall, GameBall>> = mutableListOf()

            for (ball in gameBalls) {

                // Resolving collision with edges and balls
                for (line in ALL_LINES) {
                    val fLineX1 = line.endCoordinates.x - line.startCoordinates.x
                    val fLineY1 = line.endCoordinates.y - line.startCoordinates.y


                    val fLineX2 = ball.center.x - line.startCoordinates.x
                    val fLineY2 = ball.center.y - line.startCoordinates.y

                    val fEdgeLength = fLineX1 * fLineX1 + fLineY1 * fLineY1

                    val t: Float =
                        max(
                            0f,
                            min(fEdgeLength, (fLineX1 * fLineX2 + fLineY1 * fLineY2))
                        ) / fEdgeLength

                    val fClosestPointX = line.startCoordinates.x + t * fLineX1
                    val fClosestPointY = line.startCoordinates.y + t * fLineY1

                    val fDistance =
                        sqrt((ball.center.x - fClosestPointX) * (ball.center.x - fClosestPointX) + (ball.center.y - fClosestPointY) * (ball.center.y - fClosestPointY))

                    if (fDistance <= (ball.r + line.lineRadius)) {
                        val fakeBall = GameBall(
                            center = Coordinates(fClosestPointX, fClosestPointY),
                            startCoordinates = Coordinates(fClosestPointX, fClosestPointY),
                            velocityVector = VelocityVector(
                                -ball.velocityVector.vx,
                                -ball.velocityVector.vy
                            ),
                            mass = ball.mass * 0.8f,
                            r = line.lineRadius
                        )

                        collidingBallPairs.add(Pair(ball, fakeBall))

                        val fOverlap = 1.0f * (fDistance - ball.r - fakeBall.r)
                        ball.center.x -= fOverlap * (ball.center.x - fakeBall.center.x) / fDistance
                        ball.center.y -= fOverlap * (ball.center.y - fakeBall.center.y) / fDistance

                    }

                }

                // Static collision resolution
                for (target in gameBalls) {
                    if (ball.id != target.id && ball.doesItOverlap(target)) {
                        collidingBallPairs.add(Pair(ball, target))

                        if (ball is FootballBall || target is FootballBall) {
                            onBallCollisionCallback()
                        }

                        val distance =
                            sqrt((ball.center.x - target.center.x) * (ball.center.x - target.center.x) + (ball.center.y - target.center.y) * (ball.center.y - target.center.y))

                        val overlap = 0.5f * (distance - ball.r - target.r)

                        ball.center.x -= (overlap * (ball.center.x - target.center.x) / distance)
                        ball.center.y -= (overlap * (ball.center.y - target.center.y) / distance)

                        target.center.x += (overlap * (ball.center.x - target.center.x) / distance)
                        target.center.y += (overlap * (ball.center.y - target.center.y) / distance)
                    }
                }
            }

            // Dynamic collision resolution
            for (pair in collidingBallPairs) {
                val ball1: GameBall = pair.first
                val ball2: GameBall = pair.second

                val distance: Float =
                    sqrt((ball1.center.x - ball2.center.x) * (ball1.center.x - ball2.center.x) + (ball1.center.y - ball2.center.y) * (ball1.center.y - ball2.center.y))

                val nx = (ball2.center.x - ball1.center.x) / distance
                val ny = (ball2.center.y - ball1.center.y) / distance

                val tx = -ny
                val ty = nx

                val dpTan1 = ball1.velocityVector.vx * tx + ball1.velocityVector.vy * ty
                val dpTan2 = ball2.velocityVector.vx * tx + ball2.velocityVector.vy * ty

                val dpNorm1 = ball1.velocityVector.vx * nx + ball1.velocityVector.vy * ny
                val dpNorm2 = ball2.velocityVector.vx * nx + ball2.velocityVector.vy * ny

                val m1 =
                    (dpNorm1 * (ball1.mass - ball2.mass) + 2.0f * ball2.mass * dpNorm2) / (ball1.mass + ball2.mass)
                val m2 =
                    (dpNorm2 * (ball2.mass - ball1.mass) + 2.0f * ball1.mass * dpNorm1) / (ball1.mass + ball2.mass)

                ball1.velocityVector.vx = tx * dpTan1 + nx * m1
                ball1.velocityVector.vy = ty * dpTan1 + ny * m1
                ball2.velocityVector.vx = tx * dpTan2 + nx * m2
                ball2.velocityVector.vy = ty * dpTan2 + ny * m2
            }

            GOALS.forEach { goal ->
                if (goal.isBallInGoal(ball)) {
                    changeGameState(GameStates.GOAL)
                    val player =
                        if (goal.player == Player.PLAYER_1) Player.PLAYER_2 else Player.PLAYER_1
                    result[player] = result[player]?.plus(1)

                    if (result[Player.PLAYER_1]!! == numberOfGoalsToWin || result[Player.PLAYER_2]!! == numberOfGoalsToWin) {
                        changeGameState(GameStates.FINISHED)
                    }
                    onGoalScoredCallback()
                }
            }
        }
    }
}

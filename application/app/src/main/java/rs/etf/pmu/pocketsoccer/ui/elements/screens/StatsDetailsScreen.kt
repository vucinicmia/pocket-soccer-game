package rs.etf.pmu.pocketsoccer.ui.elements.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import rs.etf.pmu.pocketsoccer.ui.elements.composables.StadiumBackground
import rs.etf.pmu.pocketsoccer.ui.stateholders.StatsViewModel
import rs.etf.pmu.pocketsoccer.utils.DateTimeUtil

@Composable
fun StatsDetailsScreen(
    viewModel: StatsViewModel,
) {
    viewModel.getGamesForPlayers()
    val uiState by viewModel.uiStateStatsScreen.collectAsState()

    val columnWeight = 0.3f

    StadiumBackground(
        verticalArrangement = Arrangement.Top
    ) {
        Text(
            text = "Results",
            modifier = Modifier
                .fillMaxWidth(),
            style = MaterialTheme.typography.titleMedium,
            textAlign = TextAlign.Center,
        )

        Spacer(modifier = Modifier.height(10.dp))

        Row(
            modifier = Modifier
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.Center
        ) {
            Text(
                text = "RESET",
                style = MaterialTheme.typography.displaySmall,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .border(2.dp, Color.LightGray, CircleShape)
                    .background(Color.LightGray.copy(alpha = 0.3f), CircleShape)
                    .padding(10.dp)
                    .clickable {
                        viewModel.deleteGamesForPlayers()
                    },
            )
        }

        Spacer(modifier = Modifier.height(10.dp))

        Row(
            modifier = Modifier
                .fillMaxWidth(),
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                TableCell(text = uiState.playerName1, weight = columnWeight, title = true)
                TableCell(text = "Finished", weight = columnWeight, title = true)
                TableCell(text = uiState.playerName2, weight = columnWeight, title = true)
            }
        }

        LazyColumn(Modifier.padding(8.dp)) {

            itemsIndexed(uiState.gameInfoList) { _, gameInfo ->

                Divider(
                    color = Color.LightGray,
                    modifier = Modifier
                        .height(1.dp)
                        .fillMaxHeight()
                        .fillMaxWidth()
                )

                Row(
                    modifier = Modifier
                        .fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    TableCell(text = "${gameInfo.playerScore1}", weight = columnWeight)
                    TableCell(text = DateTimeUtil.formatDate(gameInfo.date), weight = columnWeight)
                    TableCell(text = "${gameInfo.playerScore2}", weight = columnWeight)
                }
                Divider(
                    color = Color.LightGray,
                    modifier = Modifier
                        .height(1.dp)
                        .fillMaxHeight()
                        .fillMaxWidth()
                )
            }
        }
    }
}
package rs.etf.pmu.pocketsoccer.ui.elements.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.Dp

@Composable
fun CircularImage(
    imageId: Int,
    height: Dp,
    width: Dp
) {
    Image(
        painter = painterResource(id = imageId),
        null,
        contentScale = ContentScale.Crop,
        modifier = Modifier
            .height(height)
            .width(width)
            .clip(CircleShape)
    )
}
package rs.etf.pmu.pocketsoccer.utils

import android.app.Activity
import android.os.Build
import android.view.WindowManager
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat

object WindowUtils {

    var WIDTH: Int = 0
    var HEIGHT: Int = 0

    fun hideSystemUI(activity: Activity) {
        activity.apply {
            actionBar?.hide()

            WindowCompat.setDecorFitsSystemWindows(window, false)

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
                window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
            } else {
                with(WindowCompat.getInsetsController(window, window.decorView)) {
                    this.systemBarsBehavior =
                        WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
                    this.hide(WindowInsetsCompat.Type.systemBars())
                    this.hide(WindowInsetsCompat.Type.statusBars())
                }
            }
        }
    }

    fun initializeWidthAndHeight(activity: Activity) {
        activity.run {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                val height = windowManager.currentWindowMetrics.bounds.bottom.toFloat()
                val width = windowManager.currentWindowMetrics.bounds.right.toFloat()
                WIDTH = width.toInt()
                HEIGHT = height.toInt()
            } else {
                val displayMetrics = resources.displayMetrics
                WIDTH = displayMetrics.widthPixels
                HEIGHT = displayMetrics.heightPixels
            }
        }
    }
}
package rs.etf.pmu.pocketsoccer.game.game_objects

import rs.etf.pmu.pocketsoccer.game.utils.Coordinates
import rs.etf.pmu.pocketsoccer.utils.WindowUtils

object Field {

    private val SCREEN_WIDTH = WindowUtils.WIDTH.toFloat()
    private val SCREEN_HEIGHT = WindowUtils.HEIGHT.toFloat()

    private const val FIELD_LINE_RADIUS = 10f

    val EDGES = listOf(
        Line(
            startCoordinates = Coordinates(0f, 0f),
            endCoordinates = Coordinates(SCREEN_WIDTH, 0f),
            lineRadius = FIELD_LINE_RADIUS
        ),
        Line(
            startCoordinates = Coordinates(0f, 0f),
            endCoordinates = Coordinates(0f, SCREEN_HEIGHT),
            lineRadius = FIELD_LINE_RADIUS
        ),
        Line(
            startCoordinates = Coordinates(0f, SCREEN_HEIGHT),
            endCoordinates = Coordinates(SCREEN_WIDTH, SCREEN_HEIGHT),
            lineRadius = FIELD_LINE_RADIUS
        ),
        Line(
            startCoordinates = Coordinates(SCREEN_WIDTH, 0f),
            endCoordinates = Coordinates(SCREEN_WIDTH, SCREEN_HEIGHT),
            lineRadius = FIELD_LINE_RADIUS
        )
    )

}
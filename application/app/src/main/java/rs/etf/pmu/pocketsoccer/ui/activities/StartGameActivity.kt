package rs.etf.pmu.pocketsoccer.ui.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import dagger.hilt.android.AndroidEntryPoint
import rs.etf.pmu.pocketsoccer.R
import rs.etf.pmu.pocketsoccer.game.Game
import rs.etf.pmu.pocketsoccer.game.Player
import rs.etf.pmu.pocketsoccer.game.settings.EndGameCondition
import rs.etf.pmu.pocketsoccer.game.settings.GameMode
import rs.etf.pmu.pocketsoccer.game.settings.GameSpeed
import rs.etf.pmu.pocketsoccer.ui.elements.screens.ChoosePlayersScreen
import rs.etf.pmu.pocketsoccer.ui.elements.screens.EnterNamesScreen
import rs.etf.pmu.pocketsoccer.ui.elements.screens.NewGameScreen
import rs.etf.pmu.pocketsoccer.ui.elements.screens.SettingsScreen
import rs.etf.pmu.pocketsoccer.ui.elements.screens.StartScreen
import rs.etf.pmu.pocketsoccer.ui.elements.screens.StatsDetailsScreen
import rs.etf.pmu.pocketsoccer.ui.elements.screens.StatsScreen
import rs.etf.pmu.pocketsoccer.ui.elements.screens.settings.EndGameConditionSettingsScreen
import rs.etf.pmu.pocketsoccer.ui.elements.screens.settings.FieldSettingsScreen
import rs.etf.pmu.pocketsoccer.ui.elements.screens.settings.GameSpeedSettingsScreen
import rs.etf.pmu.pocketsoccer.ui.navigation.ChoosePlayersScreenDestination
import rs.etf.pmu.pocketsoccer.ui.navigation.EndGameConditionSettingsScreenDestination
import rs.etf.pmu.pocketsoccer.ui.navigation.EnterNamesScreenDestination
import rs.etf.pmu.pocketsoccer.ui.navigation.FieldSettingsScreenDestination
import rs.etf.pmu.pocketsoccer.ui.navigation.GameSpeedSettingsScreenDestination
import rs.etf.pmu.pocketsoccer.ui.navigation.NewGameScreenDestination
import rs.etf.pmu.pocketsoccer.ui.navigation.SettingsScreenDestination
import rs.etf.pmu.pocketsoccer.ui.navigation.StartScreenDestination
import rs.etf.pmu.pocketsoccer.ui.navigation.StatsDetailsScreenDestination
import rs.etf.pmu.pocketsoccer.ui.navigation.StatsScreenDestination
import rs.etf.pmu.pocketsoccer.ui.stateholders.StartGameActivityViewModel
import rs.etf.pmu.pocketsoccer.ui.stateholders.StatsViewModel
import rs.etf.pmu.pocketsoccer.ui.theme.PocketSoccerTheme
import rs.etf.pmu.pocketsoccer.utils.SharedPreferencesKeys
import rs.etf.pmu.pocketsoccer.utils.SharedPreferencesUtils
import rs.etf.pmu.pocketsoccer.utils.WindowUtils

@AndroidEntryPoint
class StartGameActivity : ComponentActivity() {

    private lateinit var viewModelStartGameActivity: StartGameActivityViewModel
    private lateinit var viewModelStats: StatsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            PocketSoccerTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val fromGameFinishedScreen = intent.getBooleanExtra(
                        INTENT_TAG_FROM_GAME_FINISHED_SCREEN,
                        false
                    )

                    val player1Name = intent.getStringExtra(INTENT_TAG_PLAYER_1) ?: "Player 1"
                    val player2Name = intent.getStringExtra(INTENT_TAG_PLAYER_2) ?: "Player 2"

                    viewModelStartGameActivity = viewModel()
                    viewModelStats = viewModel()

                    val startGameActivity: (Int, String, String, Int, Int) -> Unit =
                        { gameMode, playerName1, playerName2, playerFlag1, playerFlag2 ->
                            val intent = Intent(this, GameActivity::class.java).apply {
                                putExtra(GameMode.INTENT_TAG, gameMode)
                                putExtra(Player.INTENT_TAG_PLAYER_1_NAME, playerName1)
                                putExtra(Player.INTENT_TAG_PLAYER_2_NAME, playerName2)
                                putExtra(Player.INTENT_TAG_PLAYER_1_FLAG, playerFlag1)
                                putExtra(Player.INTENT_TAG_PLAYER_2_FLAG, playerFlag2)
                            }
                            intent.flags =
                                Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                            finish()
                        }

                    val continueGame: () -> Unit = {
                        val intent = Intent(this, GameActivity::class.java).apply {
                            putExtra(Game.INTENT_TAG_CONTINUE_GAME, true)
                        }
                        intent.flags =
                            Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                        finish()
                    }

                    StartGameActivityComposable(
                        viewModelStartGameActivity = viewModelStartGameActivity,
                        viewModelStats = viewModelStats,
                        startGameActivity = startGameActivity,
                        onContinueGameButtonClickCallback = continueGame,
                        fromGameFinishedScreen = fromGameFinishedScreen,
                        player1Name = player1Name,
                        player2Name = player2Name
                    )
                }
            }
        }
        WindowUtils.hideSystemUI(this)
    }

    companion object {
        const val INTENT_TAG_FROM_GAME_FINISHED_SCREEN = "from_game_finished_screen"
        const val INTENT_TAG_PLAYER_1 = "player_1"
        const val INTENT_TAG_PLAYER_2 = "player_2"
    }
}

@Composable
fun StartGameActivityComposable(
    viewModelStartGameActivity: StartGameActivityViewModel,
    viewModelStats: StatsViewModel,
    startGameActivity: (Int, String, String, Int, Int) -> Unit,
    onContinueGameButtonClickCallback: () -> Unit,
    fromGameFinishedScreen: Boolean,
    player1Name: String,
    player2Name: String
) {
    val navController = rememberNavController()
    val context = LocalContext.current

    val savedGame = SharedPreferencesUtils.getString(
        context = context,
        key = SharedPreferencesKeys.SAVED_GAME,
        defaultValue = ""
    )

    val savedGameExists = savedGame.isNotEmpty()

    NavHost(
        navController = navController,
        startDestination = StartScreenDestination.route
    ) {
        composable(route = StartScreenDestination.route) {
            StartScreen(
                savedGameExists = savedGameExists,
                onNewGameButtonClickCallback = {
                    navController.navigate(NewGameScreenDestination.route)
                },
                onContinueButtonClickCallback = onContinueGameButtonClickCallback,
                onSettingsButtonClickCallback = {
                    navController.navigate(SettingsScreenDestination.route)
                },
                onStatsButtonClickCallback = {
                    navController.navigate(StatsScreenDestination.route)
                }
            )
        }
        composable(route = SettingsScreenDestination.route) {
            SettingsScreen(
                onChooseFieldButtonClickCallback = {
                    navController.navigate(FieldSettingsScreenDestination.route)
                }, onEndGameConditionButtonClickCallback = {
                    navController.navigate(EndGameConditionSettingsScreenDestination.route)
                }, onGameSpeedButtonClickCallback = {
                    navController.navigate(GameSpeedSettingsScreenDestination.route)
                }
            )
        }
        composable(route = FieldSettingsScreenDestination.route) {
            val fieldId = SharedPreferencesUtils.getInt(
                context = context,
                key = SharedPreferencesKeys.SETTINGS_FIELD,
                defaultValue = R.drawable.field_grass
            )
            FieldSettingsScreen(
                currentlySelectedField = fieldId,
                onFieldChangedCallback = { id ->
                    SharedPreferencesUtils.putInt(
                        context = context,
                        key = SharedPreferencesKeys.SETTINGS_FIELD,
                        data = id
                    )
                }
            )
        }
        composable(route = EndGameConditionSettingsScreenDestination.route) {
            val endGameCondition = SharedPreferencesUtils.getInt(
                context = context,
                key = SharedPreferencesKeys.SETTINGS_END_GAME_CONDITION,
                defaultValue = EndGameCondition.TIMEOUT.ordinal
            )
            val numberOfGoalsFromSP = SharedPreferencesUtils.getInt(
                context = context,
                key = SharedPreferencesKeys.SETTINGS_NUMBER_OF_GOALS,
                defaultValue = EndGameCondition.DEFAULT_NUMBER_OF_GOALS
            )
            val timeoutFromSP = SharedPreferencesUtils.getLong(
                context = context,
                key = SharedPreferencesKeys.SETTINGS_TIMEOUT,
                defaultValue = EndGameCondition.DEFAULT_TIMEOUT
            )
            EndGameConditionSettingsScreen(
                numberOfGoals = numberOfGoalsFromSP,
                timeout = timeoutFromSP,
                currentlySelectedEndGameCondition = EndGameCondition.fromInt(endGameCondition),
                onChangedCallback = { condition, numberOfGoals, timeout ->
                    SharedPreferencesUtils.putInt(
                        context = context,
                        key = SharedPreferencesKeys.SETTINGS_END_GAME_CONDITION,
                        data = condition.ordinal
                    )
                    numberOfGoals?.let {
                        SharedPreferencesUtils.putInt(
                            context = context,
                            key = SharedPreferencesKeys.SETTINGS_NUMBER_OF_GOALS,
                            data = it
                        )
                    }
                    timeout?.let {
                        SharedPreferencesUtils.putLong(
                            context = context,
                            key = SharedPreferencesKeys.SETTINGS_TIMEOUT,
                            data = it
                        )
                    }
                }
            )
        }
        composable(route = GameSpeedSettingsScreenDestination.route) {
            val currentGameSpeed = SharedPreferencesUtils.getInt(
                context = context,
                key = SharedPreferencesKeys.SETTINGS_GAME_SPEED,
                defaultValue = GameSpeed.NORMAL.ordinal
            )
            GameSpeedSettingsScreen(
                currentGameSpeed = GameSpeed.fromInt(currentGameSpeed),
                onChangedCallback = { gameSpeed ->
                    SharedPreferencesUtils.putInt(
                        context = context,
                        key = SharedPreferencesKeys.SETTINGS_GAME_SPEED,
                        data = gameSpeed.ordinal
                    )
                })
        }
        composable(route = NewGameScreenDestination.route) {
            NewGameScreen(
                onSinglePlayerButtonClickCallback = {
                    navController.navigate("${EnterNamesScreenDestination.route}/0")
                }, onMultiPlayerButtonClickCallback = {
                    navController.navigate("${EnterNamesScreenDestination.route}/1")
                }
            )
        }
        composable(
            route = "${EnterNamesScreenDestination.route}/{gameMode}",
            arguments = listOf(navArgument("gameMode") { type = NavType.IntType })
        ) { navBackStackEntry ->
            val gameMode = navBackStackEntry.arguments?.getInt("gameMode") ?: 0
            EnterNamesScreen(
                viewModel = viewModelStartGameActivity,
                onContinueButtonClickCallback = {
                    if (viewModelStartGameActivity.checkIfNamesAreEntered()) {
                        navController.navigate("${ChoosePlayersScreenDestination.route}/$gameMode")
                    } else {
                        Toast.makeText(
                            context,
                            "Please enter names for both players",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            )
        }
        composable(
            route = "${ChoosePlayersScreenDestination.route}/{gameMode}",
            arguments = listOf(navArgument("gameMode") { type = NavType.IntType })
        ) { navBackStackEntry ->
            val gameMode = navBackStackEntry.arguments?.getInt("gameMode") ?: 0
            ChoosePlayersScreen(
                gameMode = gameMode,
                viewModel = viewModelStartGameActivity,
                onPlayButtonClickCallback = startGameActivity
            )
        }
        composable(route = StatsScreenDestination.route) {
            StatsScreen(
                viewModel = viewModelStats,
                onRowClickedCallback = {
                    navController.navigate(StatsDetailsScreenDestination.route)
                }
            )
        }
        composable(route = StatsDetailsScreenDestination.route) {
            StatsDetailsScreen(
                viewModel = viewModelStats,
            )
        }
    }

    if (fromGameFinishedScreen) {
        viewModelStats.setPlayer1(player1Name)
        viewModelStats.setPlayer2(player2Name)
        navController.navigate(StartScreenDestination.route)
        navController.navigate(StatsDetailsScreenDestination.route) {
            popUpTo(StartScreenDestination.route) {
                inclusive = true
            }
        }
    }
}
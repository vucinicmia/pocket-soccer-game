package rs.etf.pmu.pocketsoccer.ui.elements.screens.game

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import rs.etf.pmu.pocketsoccer.ui.elements.composables.game.DarkenedLayer

@Composable
fun GameStartScreen(
    onStartClickedCallback: () -> Unit
) {
    DarkenedLayer()

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Text(
            text = "START",
            style = MaterialTheme.typography.displayLarge,
            modifier = Modifier
                .clickable(onClick = onStartClickedCallback)
        )
    }
}
package rs.etf.pmu.pocketsoccer.game.utils

import android.content.*

class SharedPreferenceLoader(
    private var context: Context, private var fileName: String
) {
    fun save(key: String, data: String) {
        val preferences = context.getSharedPreferences(fileName, Context.MODE_PRIVATE)
        val editor = preferences.edit()
        with(editor) {
            putString(key, data)
            // commit() //writes immediately
            apply() //writes in background
        }
    }

    fun load(key: String): String {
        val preferences = context.getSharedPreferences(fileName, Context.MODE_PRIVATE)
        return preferences.getString(key, DATA_DEFAULT_VALUE)!!
    }

    companion object {
        const val KEY_REMEMBER_LOGIN = "REMEMBER_LOGIN"
        const val DATA_DEFAULT_VALUE = ""
    }
}
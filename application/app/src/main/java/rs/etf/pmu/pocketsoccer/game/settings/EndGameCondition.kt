package rs.etf.pmu.pocketsoccer.game.settings

enum class EndGameCondition {
    TIMEOUT, NUMBER_OF_GOALS;

    companion object {

        const val DEFAULT_NUMBER_OF_GOALS = 3
        const val DEFAULT_TIMEOUT = 30000L // 30 seconds

        fun fromInt(value: Int) = when (value) {
            0 -> TIMEOUT
            1 -> NUMBER_OF_GOALS
            else -> throw IllegalArgumentException()
        }

        fun toInt(value: EndGameCondition) = when (value) {
            TIMEOUT -> 0
            NUMBER_OF_GOALS -> 1
        }
    }
}
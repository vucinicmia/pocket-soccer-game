package rs.etf.pmu.pocketsoccer.data.room.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user")
data class User(
    @PrimaryKey val username: String,
    val password: String,
    @ColumnInfo(name = "photo_path") val photoPath: String
)
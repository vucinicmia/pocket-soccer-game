package rs.etf.pmu.pocketsoccer.game.settings

enum class GameSpeed {
    SLOW, NORMAL, FAST;

    companion object {

        fun fromInt(value: Int) = when (value) {
            0 -> SLOW
            1 -> NORMAL
            2 -> FAST
            else -> throw IllegalArgumentException()
        }

        fun toFloat(value: GameSpeed) = when (value) {
            SLOW -> 0.5f
            NORMAL -> 1f
            FAST -> 5f
        }
    }
}
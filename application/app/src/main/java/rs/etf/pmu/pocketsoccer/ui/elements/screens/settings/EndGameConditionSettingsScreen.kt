package rs.etf.pmu.pocketsoccer.ui.elements.screens.settings

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import rs.etf.pmu.pocketsoccer.game.settings.EndGameCondition
import rs.etf.pmu.pocketsoccer.ui.applyIf
import rs.etf.pmu.pocketsoccer.ui.elements.composables.InputTextField
import rs.etf.pmu.pocketsoccer.ui.elements.composables.StadiumBackground

@Composable
fun EndGameConditionSettingsScreen(
    currentlySelectedEndGameCondition: EndGameCondition,
    numberOfGoals: Int,
    timeout: Long,
    onChangedCallback: (EndGameCondition, Int?, Long?) -> Unit
) {
    var selectedEndGameCondition by rememberSaveable {
        mutableStateOf(currentlySelectedEndGameCondition)
    }

    var numberOfGoalsTextFieldValue by rememberSaveable {
        mutableStateOf("$numberOfGoals")
    }

    var timeoutTextFieldValue by rememberSaveable {
        mutableStateOf("$timeout")
    }

    StadiumBackground {
        Text(
            text = "Choose end game condiotion",
            modifier = Modifier
                .fillMaxWidth(),
            style = MaterialTheme.typography.titleMedium,
            textAlign = TextAlign.Center,
        )

        Row(
            modifier = Modifier
                .fillMaxSize()
                .padding(start = 50.dp, end = 50.dp),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        ) {

            Column(
                modifier = Modifier.weight(1f),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                val borderWidthNumberOfGoals =
                    if (selectedEndGameCondition == EndGameCondition.NUMBER_OF_GOALS) 2.dp else 1.dp
                Text(
                    text = "GOALS",
                    style = MaterialTheme.typography.displayLarge,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .border(borderWidthNumberOfGoals, Color.LightGray, CircleShape)
                        .applyIf(selectedEndGameCondition == EndGameCondition.NUMBER_OF_GOALS) {
                            background(Color.LightGray.copy(alpha = 0.3f), CircleShape)
                        }
                        .padding(10.dp)
                        .clickable {
                            selectedEndGameCondition = EndGameCondition.NUMBER_OF_GOALS
                            onChangedCallback(
                                selectedEndGameCondition,
                                numberOfGoalsTextFieldValue.toIntOrNull(),
                                null
                            )
                        },
                )

                Spacer(modifier = Modifier.height(20.dp))

                InputTextField(
                    text = numberOfGoalsTextFieldValue,
                    onValueChange = {
                        numberOfGoalsTextFieldValue = it
                        onChangedCallback(selectedEndGameCondition, it.toIntOrNull(), null)
                    }, imeAction = ImeAction.Done,
                    keyboardType = KeyboardType.Number,
                    enabled = selectedEndGameCondition == EndGameCondition.NUMBER_OF_GOALS,
                    textStyle = MaterialTheme.typography.displayMedium
                )
            }

            Column(
                modifier = Modifier.weight(1f),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                val borderWidthTimeout =
                    if (selectedEndGameCondition == EndGameCondition.TIMEOUT) 2.dp else 1.dp
                Text(
                    text = "TIMEOUT",
                    style = MaterialTheme.typography.displayLarge,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .border(borderWidthTimeout, Color.LightGray, CircleShape)
                        .applyIf(selectedEndGameCondition == EndGameCondition.TIMEOUT) {
                            background(Color.LightGray.copy(alpha = 0.3f), CircleShape)
                        }
                        .padding(10.dp)
                        .clickable {
                            selectedEndGameCondition = EndGameCondition.TIMEOUT
                            onChangedCallback(
                                selectedEndGameCondition,
                                null,
                                timeoutTextFieldValue.toLongOrNull()
                            )
                        },
                )

                Spacer(modifier = Modifier.height(20.dp))

                InputTextField(
                    text = timeoutTextFieldValue,
                    onValueChange = {
                        timeoutTextFieldValue = it
                        onChangedCallback(selectedEndGameCondition, null, it.toLongOrNull())
                    }, imeAction = ImeAction.Done,
                    keyboardType = KeyboardType.Number,
                    enabled = selectedEndGameCondition == EndGameCondition.TIMEOUT,
                    textStyle = MaterialTheme.typography.displayMedium
                )
            }
        }
    }
}
package rs.etf.pmu.pocketsoccer.ui.elements.screens.game

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.sp
import rs.etf.pmu.pocketsoccer.ui.elements.composables.game.DarkenedLayer

@Composable
fun GameFinishedScreen(
    playerResult1: Int,
    playerResult2: Int,
    onFinishedGameButtonClickCallback: () -> Unit,
) {
    DarkenedLayer()

    val message =
        if (playerResult1 > playerResult2)
            "PLAYER 1 WON"
        else if (playerResult1 < playerResult2)
            "PLAYER 2 WON"
        else
            "TIE"

    Box(modifier = Modifier.fillMaxSize()) {
        Text(
            text = message,
            modifier = Modifier
                .align(Alignment.Center)
                .clickable(onClick = onFinishedGameButtonClickCallback),
            style = MaterialTheme.typography.displayLarge
        )
    }
}
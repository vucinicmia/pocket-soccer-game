package rs.etf.pmu.pocketsoccer.ui.elements.screens

import androidx.compose.foundation.layout.*
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import rs.etf.pmu.pocketsoccer.ui.elements.composables.CheckBox
import rs.etf.pmu.pocketsoccer.ui.elements.composables.GrayButton
import rs.etf.pmu.pocketsoccer.ui.elements.composables.InputTextField
import rs.etf.pmu.pocketsoccer.ui.elements.composables.StadiumBackground
import rs.etf.pmu.pocketsoccer.ui.stateholders.StartActivityViewModel

@Composable
fun LogInScreen(
    viewModel: StartActivityViewModel,
    onSuccessfulLogInCallback: () -> Unit = {},
    onErrorCallback: (String) -> Unit = {}
) {
    val uiState by viewModel.uiStateLoginScreen.collectAsState()

    StadiumBackground {
        Text(
            text = "Log In",
            modifier = Modifier
                .fillMaxWidth(),
            style = MaterialTheme.typography.titleLarge,
            textAlign = TextAlign.Center,
        )

        Spacer(modifier = Modifier.height(20.dp))

        InputTextField(
            modifier = Modifier.fillMaxWidth(),
            label = "Username",
            text = uiState.username,
            onValueChange = viewModel::updateUsernameLogInScreen,
            imeAction = ImeAction.Next
        )

        Spacer(modifier = Modifier.height(20.dp))

        InputTextField(
            modifier = Modifier.fillMaxWidth(),
            label = "Password",
            text = uiState.password,
            onValueChange = viewModel::updatePasswordLogInScreen,
            imeAction = ImeAction.Done,
            password = true
        )

        Row(
            modifier = Modifier.fillMaxSize().padding(start = 30.dp, end = 30.dp),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        ) {
            CheckBox(
                modifier = Modifier.weight(1f),
                checked = uiState.rememberLogin,
                onCheckedChange = viewModel::updateRememberLogin,
                text = "Remember me"
            )
            GrayButton(
                text = "Log In",
                onClick = { viewModel.login(onSuccessfulLogInCallback, onErrorCallback) },
                modifier = Modifier.weight(1f)
            )
        }
    }
}
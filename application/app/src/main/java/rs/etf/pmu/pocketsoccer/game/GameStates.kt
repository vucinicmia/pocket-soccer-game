package rs.etf.pmu.pocketsoccer.game

enum class GameStates {
    RUNNING, PAUSED, GOAL, NOT_STARTED, FINISHED
}
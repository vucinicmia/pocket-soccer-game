package rs.etf.pmu.pocketsoccer.ui.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import androidx.lifecycle.viewmodel.compose.viewModel
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import rs.etf.pmu.pocketsoccer.R
import rs.etf.pmu.pocketsoccer.game.Game
import rs.etf.pmu.pocketsoccer.game.GameState
import rs.etf.pmu.pocketsoccer.game.Player
import rs.etf.pmu.pocketsoccer.game.settings.EndGameCondition
import rs.etf.pmu.pocketsoccer.game.settings.GameMode
import rs.etf.pmu.pocketsoccer.game.settings.GameSpeed
import rs.etf.pmu.pocketsoccer.ui.elements.screens.game.GameScreen
import rs.etf.pmu.pocketsoccer.ui.stateholders.GameViewModel
import rs.etf.pmu.pocketsoccer.ui.stateholders.StatsViewModel
import rs.etf.pmu.pocketsoccer.ui.theme.PocketSoccerTheme
import rs.etf.pmu.pocketsoccer.utils.SharedPreferencesKeys
import rs.etf.pmu.pocketsoccer.utils.SharedPreferencesUtils
import rs.etf.pmu.pocketsoccer.utils.WindowUtils

@AndroidEntryPoint
class GameActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            PocketSoccerTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier
                        .fillMaxSize(),
                ) {
                    val gameViewModel: GameViewModel = viewModel()
                    val viewModelStats: StatsViewModel = viewModel()

                    val continueGame = intent.getBooleanExtra(Game.INTENT_TAG_CONTINUE_GAME, false)

                    if (continueGame) {
                        val gameStateJSON = SharedPreferencesUtils.getString(
                            context = this@GameActivity,
                            key = SharedPreferencesKeys.SAVED_GAME,
                            defaultValue = ""
                        )

                        val gameState = Gson().fromJson(gameStateJSON, GameState::class.java)
                        Log.d("GameActivity", "onCreate: $gameState")
                        gameViewModel.restoreGame(gameState = gameState)

                    } else {
                        val gameMode =
                            intent.getIntExtra(GameMode.INTENT_TAG, GameMode.MULTIPLAYER.ordinal)
                        val playerName1 = intent.getStringExtra(Player.INTENT_TAG_PLAYER_1_NAME)
                            ?: Player.PLAYER_1_DEFAULT_NAME
                        val playerName2 = intent.getStringExtra(Player.INTENT_TAG_PLAYER_2_NAME)
                            ?: Player.PLAYER_2_DEFAULT_NAME
                        val playerFlag1 =
                            intent.getIntExtra(Player.INTENT_TAG_PLAYER_1_FLAG, R.drawable.ar)
                        val playerFlag2 =
                            intent.getIntExtra(Player.INTENT_TAG_PLAYER_2_FLAG, R.drawable.am)

                        val endGameCondition = SharedPreferencesUtils.getInt(
                            context = this@GameActivity,
                            key = SharedPreferencesKeys.SETTINGS_END_GAME_CONDITION,
                            defaultValue = EndGameCondition.TIMEOUT.ordinal
                        )

                        val numberOfGoals = SharedPreferencesUtils.getInt(
                            context = this@GameActivity,
                            key = SharedPreferencesKeys.SETTINGS_NUMBER_OF_GOALS,
                            defaultValue = EndGameCondition.DEFAULT_NUMBER_OF_GOALS
                        )

                        val timeout = SharedPreferencesUtils.getLong(
                            context = this@GameActivity,
                            key = SharedPreferencesKeys.SETTINGS_TIMEOUT,
                            defaultValue = EndGameCondition.DEFAULT_TIMEOUT
                        )

                        val fieldImage = SharedPreferencesUtils.getInt(
                            context = this@GameActivity,
                            key = SharedPreferencesKeys.SETTINGS_FIELD,
                            defaultValue = R.drawable.field_grass
                        )

                        val gameSpeed = SharedPreferencesUtils.getInt(
                            context = this@GameActivity,
                            key = SharedPreferencesKeys.SETTINGS_GAME_SPEED,
                            defaultValue = GameSpeed.NORMAL.ordinal
                        )

                        gameViewModel.start(
                            gameMode = GameMode.fromInt(gameMode),
                            playerName1 = playerName1,
                            playerName2 = playerName2,
                            playerFlag1 = playerFlag1,
                            playerFlag2 = playerFlag2,
                            endGameCondition = EndGameCondition.fromInt(endGameCondition),
                            numberOfGoals = numberOfGoals,
                            timeout = timeout,
                            fieldImageId = fieldImage,
                            gameSpeed = GameSpeed.fromInt(gameSpeed)
                        )
                    }

                    GameScreen(
                        viewModel = gameViewModel,
                        onSaveAndExitGameButtonClickCallback = {
                            gameViewModel.cancelJob()

                            val gson = Gson()
                            val gameStateJSON: String = gson.toJson(Game.gameState)

                            SharedPreferencesUtils.putString(
                                context = this@GameActivity,
                                key = SharedPreferencesKeys.SAVED_GAME,
                                data = gameStateJSON
                            )
                            finish()
                        },
                        onExitGameButtonClickCallback = {
                            gameViewModel.cancelJob()
                            finish()
                        },
                        onFinishedGameButtonClickCallback = {
                            gameViewModel.cancelJob()
                            with(Game.gameState) {
                                viewModelStats.saveGame(
                                    playerName1 = playerName1,
                                    playerName2 = playerName2,
                                    playerScore1 = result[Player.PLAYER_1]!!,
                                    playerScore2 = result[Player.PLAYER_2]!!
                                )
                            }
                            SharedPreferencesUtils.remove(
                                context = this@GameActivity,
                                key = SharedPreferencesKeys.SAVED_GAME
                            )
                            val intent = Intent(this, StartGameActivity::class.java).apply {
                                putExtra(
                                    StartGameActivity.INTENT_TAG_FROM_GAME_FINISHED_SCREEN,
                                    true
                                )
                                putExtra(
                                    StartGameActivity.INTENT_TAG_PLAYER_1,
                                    Game.gameState.playerName1
                                )
                                putExtra(
                                    StartGameActivity.INTENT_TAG_PLAYER_2,
                                    Game.gameState.playerName2
                                )
                            }
                            intent.flags =
                                Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                            finish()
                        },
                    )
                }
            }
        }

        WindowUtils.hideSystemUI(this)
    }
}
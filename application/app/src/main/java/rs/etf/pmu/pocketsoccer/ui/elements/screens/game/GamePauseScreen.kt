package rs.etf.pmu.pocketsoccer.ui.elements.screens.game

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import rs.etf.pmu.pocketsoccer.ui.elements.composables.game.DarkenedLayer

@Composable
fun GamePauseScreen(
    onResumeButtonClickCallback: () -> Unit,
    onSaveAndExitGameButtonClickCallback: () -> Unit,
    onExitGameButtonClickCallback: () -> Unit
) {
    DarkenedLayer()

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(10.dp),
            verticalArrangement = Arrangement.SpaceEvenly,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = "RESUME",
                style = MaterialTheme.typography.displayLarge,
                modifier = Modifier
                    .clickable(onClick = onResumeButtonClickCallback)
            )
            Text(
                text = "SAVE AND EXIT",
                style = MaterialTheme.typography.displayLarge,
                modifier = Modifier
                    .clickable(onClick = onSaveAndExitGameButtonClickCallback)
            )
            Text(
                text = "EXIT",
                style = MaterialTheme.typography.displayLarge,
                modifier = Modifier
                    .clickable(onClick = onExitGameButtonClickCallback)
            )
        }
    }
}
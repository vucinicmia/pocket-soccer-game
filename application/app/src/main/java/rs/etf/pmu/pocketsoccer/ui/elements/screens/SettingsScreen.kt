package rs.etf.pmu.pocketsoccer.ui.elements.screens

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import rs.etf.pmu.pocketsoccer.ui.elements.composables.GrayButton
import rs.etf.pmu.pocketsoccer.ui.elements.composables.StadiumBackground

@Composable
fun SettingsScreen(
    onChooseFieldButtonClickCallback: () -> Unit,
    onEndGameConditionButtonClickCallback: () -> Unit,
    onGameSpeedButtonClickCallback: () -> Unit
) {
    val buttonsDetailsMap = mapOf(
        "Field settings" to onChooseFieldButtonClickCallback,
        "End game condition" to onEndGameConditionButtonClickCallback,
        "Speed settings" to onGameSpeedButtonClickCallback
    )

    StadiumBackground {
        Column(
            modifier = Modifier
                .fillMaxSize()
        ) {
            Row(
                modifier = Modifier
                    .weight(1 / 4f)
                    .fillMaxSize(),
                verticalAlignment = Alignment.Bottom
            ) {
                Text(
                    text = "Settings",
                    modifier = Modifier
                        .fillMaxWidth(),
                    style = MaterialTheme.typography.titleMedium,
                    textAlign = TextAlign.Center,
                )
            }

            Row(
                modifier = Modifier
                    .weight(3 / 4f)
                    .fillMaxSize(),
            ) {
                for ((displayText, callback) in buttonsDetailsMap) {
                    Column(
                        modifier = Modifier
                            .weight(1f)
                            .fillMaxHeight()
                            .padding(30.dp),
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center
                    ) {
                        GrayButton(
                            text = displayText,
                            textStyle = MaterialTheme.typography.displayMedium,
                            onClick = callback
                        )
                    }
                }
            }
        }
    }
}
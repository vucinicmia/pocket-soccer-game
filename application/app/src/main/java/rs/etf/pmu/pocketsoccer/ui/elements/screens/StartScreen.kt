package rs.etf.pmu.pocketsoccer.ui.elements.screens

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import rs.etf.pmu.pocketsoccer.R
import rs.etf.pmu.pocketsoccer.ui.elements.composables.CircleShapedButton
import rs.etf.pmu.pocketsoccer.ui.elements.composables.GrayButton
import rs.etf.pmu.pocketsoccer.ui.elements.composables.StadiumBackground

@Composable
fun StartScreen(
    savedGameExists: Boolean,
    onNewGameButtonClickCallback: () -> Unit,
    onContinueButtonClickCallback: () -> Unit,
    onSettingsButtonClickCallback: () -> Unit,
    onStatsButtonClickCallback: () -> Unit
) {
    StadiumBackground {
        Column(
            modifier = Modifier
                .fillMaxSize()
        ) {
            Row(
                modifier = Modifier
                    .weight(1 / 4f)
                    .fillMaxSize(),
                verticalAlignment = Alignment.Bottom
            ) {
                Text(
                    text = "Pocket Soccer",
                    modifier = Modifier
                        .fillMaxWidth(),
                    style = MaterialTheme.typography.titleMedium,
                    textAlign = TextAlign.Center,
                )
            }

            Row(
                modifier = Modifier
                    .weight(3 / 4f)
                    .fillMaxSize(),
            ) {
                Column(
                    modifier = Modifier
                        .weight(1f)
                        .fillMaxHeight()
                        .padding(30.dp),
                    horizontalAlignment = Alignment.Start,
                    verticalArrangement = Arrangement.Bottom
                ) {
                    CircleShapedButton(
                        imageId = R.drawable.baseline_bar_chart_24,
                        height = 80.dp,
                        width = 80.dp,
                        backgroundColor = Color.LightGray,
                        onClick = onStatsButtonClickCallback
                    )
                }

                Column(
                    modifier = Modifier
                        .weight(1f)
                        .fillMaxHeight(),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    GrayButton(
                        text = "New game",
                        onClick = onNewGameButtonClickCallback
                    )

                    Spacer(modifier = Modifier.height(30.dp))

                    if (savedGameExists) {
                        GrayButton(
                            text = "Continue",
                            onClick = onContinueButtonClickCallback
                        )
                    }
                }

                Column(
                    modifier = Modifier
                        .weight(1f)
                        .fillMaxHeight()
                        .padding(30.dp),
                    horizontalAlignment = Alignment.End,
                    verticalArrangement = Arrangement.Bottom
                ) {
                    CircleShapedButton(
                        imageId = R.drawable.baseline_settings_24,
                        height = 80.dp,
                        width = 80.dp,
                        backgroundColor = Color.LightGray,
                        onClick = onSettingsButtonClickCallback
                    )
                }
            }
        }
    }
}
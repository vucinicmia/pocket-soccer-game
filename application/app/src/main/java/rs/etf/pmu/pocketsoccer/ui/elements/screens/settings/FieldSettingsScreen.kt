package rs.etf.pmu.pocketsoccer.ui.elements.screens.settings

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import rs.etf.pmu.pocketsoccer.R
import rs.etf.pmu.pocketsoccer.ui.elements.composables.StadiumBackground

@Composable
fun FieldSettingsScreen(
    currentlySelectedField: Int,
    onFieldChangedCallback: (Int) -> Unit
) {
    val fields = arrayOf(
        R.drawable.field_grass,
        R.drawable.field_concrete,
        R.drawable.field_parquet
    )

    var selectedFieldIndex by rememberSaveable {
        mutableIntStateOf(fields.indexOf(currentlySelectedField))
    }

    StadiumBackground {
        Text(
            text = "Choose field",
            modifier = Modifier
                .fillMaxWidth(),
            style = MaterialTheme.typography.titleMedium,
            textAlign = TextAlign.Center,
        )

        Spacer(modifier = Modifier.height(10.dp))

        Row(
            modifier = Modifier
                .fillMaxSize()
                .padding(20.dp),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Image(
                painter = painterResource(id = R.drawable.baseline_arrow_left_24),
                null,
                contentScale = ContentScale.FillBounds,
                modifier = Modifier
                    .clickable(onClick = {
                        selectedFieldIndex = (selectedFieldIndex - 1 + fields.size) % fields.size
                        onFieldChangedCallback(fields[selectedFieldIndex])
                    })
            )
            Image(
                painter = painterResource(id = fields[selectedFieldIndex]),
                null,
                contentScale = ContentScale.FillHeight,
                modifier = Modifier
                    .fillMaxHeight()
            )
            Image(
                painter = painterResource(id = R.drawable.baseline_arrow_right_24),
                null,
                contentScale = ContentScale.FillBounds,
                modifier = Modifier
                    .clickable(onClick = {
                        selectedFieldIndex = (selectedFieldIndex + 1) % fields.size
                        onFieldChangedCallback(fields[selectedFieldIndex])
                    })
            )
        }
    }
}
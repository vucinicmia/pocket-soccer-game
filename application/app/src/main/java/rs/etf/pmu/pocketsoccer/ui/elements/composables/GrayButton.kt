package rs.etf.pmu.pocketsoccer.ui.elements.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp

@Composable
fun GrayButton(
    modifier: Modifier = Modifier,
    text: String,
    textStyle: TextStyle = MaterialTheme.typography.displayLarge,
    backgroundColor: Color = Color.DarkGray,
    onClick: () -> Unit
) {
    Card(
        modifier = Modifier
            .clip(RoundedCornerShape(40))
            .then(modifier)
            .clickable { onClick() }
            .shadow(
                elevation = 10.dp,
                shape = RectangleShape
            )
            .border(
                width = 2.dp,
                color = Color.Black,
                shape = RoundedCornerShape(40)
            )
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .background(backgroundColor)
        ) {
            Text(
                text = text,
                modifier = Modifier
                    .fillMaxWidth(),
                style = textStyle,
                textAlign = TextAlign.Center,
            )
        }
    }
}
package rs.etf.pmu.pocketsoccer.ui.elements.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import rs.etf.pmu.pocketsoccer.R
import rs.etf.pmu.pocketsoccer.ui.elements.composables.CircularImage
import rs.etf.pmu.pocketsoccer.ui.elements.composables.GrayButton
import rs.etf.pmu.pocketsoccer.ui.elements.composables.StadiumBackground
import rs.etf.pmu.pocketsoccer.ui.stateholders.StartGameActivityViewModel
import rs.etf.pmu.pocketsoccer.utils.FlagsResource

@Composable
fun ChoosePlayersScreen(
    gameMode: Int,
    viewModel: StartGameActivityViewModel,
    onPlayButtonClickCallback: (Int, String, String, Int, Int) -> Unit
) {
    val uiStateChoosePlayersScreen by viewModel.uiStateChoosePlayersScreen.collectAsState()
    val uiStateEnterNamesScreen by viewModel.uiStateEnterNamesScreen.collectAsState()

    StadiumBackground {
        Column(
            modifier = Modifier
                .fillMaxSize()
        ) {
            Row(
                modifier = Modifier
                    .weight(1 / 4f)
                    .fillMaxSize(),
                verticalAlignment = Alignment.Bottom
            ) {
                Text(
                    text = "Choose players",
                    modifier = Modifier
                        .fillMaxWidth(),
                    style = MaterialTheme.typography.titleMedium,
                    textAlign = TextAlign.Center,
                )
            }

            Row(
                modifier = Modifier
                    .weight(3 / 4f)
                    .fillMaxSize(),
            ) {
                Column(
                    modifier = Modifier
                        .weight(1f)
                        .fillMaxHeight(),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.baseline_arrow_drop_up_24),
                        null,
                        contentScale = ContentScale.FillBounds,
                        modifier = Modifier.clickable(onClick = viewModel::arrowUpPlayer1)
                    )
                    CircularImage(
                        imageId = FlagsResource.allFlags[uiStateChoosePlayersScreen.playerFlagIndex1],
                        height = 100.dp,
                        width = 100.dp
                    )
                    Image(
                        painter = painterResource(id = R.drawable.baseline_arrow_drop_down_24),
                        null,
                        contentScale = ContentScale.FillBounds,
                        modifier = Modifier.clickable(onClick = viewModel::arrowDownPlayer1)
                    )
                }

                Column(
                    modifier = Modifier
                        .weight(1f)
                        .fillMaxHeight(),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    GrayButton(
                        text = "Play",
                        onClick = {
                            onPlayButtonClickCallback(
                                gameMode,
                                uiStateEnterNamesScreen.playerName1,
                                uiStateEnterNamesScreen.playerName2,
                                FlagsResource.allFlags[uiStateChoosePlayersScreen.playerFlagIndex1],
                                FlagsResource.allFlags[uiStateChoosePlayersScreen.playerFlagIndex2]
                            )
                        }
                    )
                }

                Column(
                    modifier = Modifier
                        .weight(1f)
                        .fillMaxHeight(),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.baseline_arrow_drop_up_24),
                        null,
                        contentScale = ContentScale.FillBounds,
                        modifier = Modifier
                            .clickable(onClick = viewModel::arrowUpPlayer2)
                    )
                    CircularImage(
                        imageId = FlagsResource.allFlags[uiStateChoosePlayersScreen.playerFlagIndex2],
                        height = 100.dp,
                        width = 100.dp
                    )
                    Image(
                        painter = painterResource(id = R.drawable.baseline_arrow_drop_down_24),
                        null,
                        contentScale = ContentScale.FillBounds,
                        modifier = Modifier.clickable(onClick = viewModel::arrowDownPlayer2)
                    )
                }
            }
        }
    }
}
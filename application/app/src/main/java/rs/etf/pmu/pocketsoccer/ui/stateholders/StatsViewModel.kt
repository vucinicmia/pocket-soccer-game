package rs.etf.pmu.pocketsoccer.ui.stateholders

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import rs.etf.pmu.pocketsoccer.data.room.entities.GameInfo
import rs.etf.pmu.pocketsoccer.repositories.GameInfoRepository
import java.util.Date
import javax.inject.Inject

data class GameInfoUiState(
    val gameInfoList: List<GameInfo> = listOf(),
    val playerName1: String = "",
    val playerName2: String = ""
)

@HiltViewModel
class StatsViewModel @Inject constructor(
    private val gameInfoRepository: GameInfoRepository
) : ViewModel() {

    private val _uiStateStatsScreen = MutableStateFlow(GameInfoUiState())
    val uiStateStatsScreen = _uiStateStatsScreen.asStateFlow()

    fun getGameInfo() {
        val gameInfoListDeferred =
            viewModelScope.async(Dispatchers.IO) { gameInfoRepository.getAllGames() }

        viewModelScope.launch(Dispatchers.IO) {
            gameInfoListDeferred.await().let { gameInfoList ->
                _uiStateStatsScreen.update {
                    it.copy(
                        gameInfoList = gameInfoList
                    )
                }
            }
        }
    }

    fun getGamesForPlayers() {
        with(uiStateStatsScreen.value) {
            val gameInfoListDeferred =
                viewModelScope.async(Dispatchers.IO) {
                    gameInfoRepository.getAllGamesForPlayers(
                        playerName1,
                        playerName2
                    )
                }

            viewModelScope.launch(Dispatchers.IO) {
                gameInfoListDeferred.await().let { gameInfoList ->
                    _uiStateStatsScreen.update {
                        it.copy(
                            gameInfoList = gameInfoList
                        )
                    }
                }
            }
        }
    }

    fun setPlayer1(playerName1: String) {
        _uiStateStatsScreen.update {
            it.copy(
                playerName1 = playerName1
            )
        }
    }

    fun setPlayer2(playerName2: String) {
        _uiStateStatsScreen.update {
            it.copy(
                playerName2 = playerName2
            )
        }
    }

    fun deleteAllGames() {
        viewModelScope.launch(Dispatchers.IO) {
            gameInfoRepository.deleteAllGames()
        }
        getGameInfo()
    }

    fun deleteGamesForPlayers() {
        viewModelScope.launch(Dispatchers.IO) {
            with(uiStateStatsScreen.value) {
                gameInfoRepository.deleteAllGamesForPlayers(playerName1, playerName2)
            }
        }
        getGamesForPlayers()
    }

    fun saveGame(playerName1: String, playerName2: String, playerScore1: Int, playerScore2: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            gameInfoRepository.insertGameInfo(
                GameInfo(
                    playerName1 = playerName1,
                    playerName2 = playerName2,
                    playerScore1 = playerScore1,
                    playerScore2 = playerScore2,
                    date = Date()
                )
            )
        }
    }
}
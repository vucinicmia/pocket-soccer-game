package rs.etf.pmu.pocketsoccer.ui.elements.screens

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import rs.etf.pmu.pocketsoccer.ui.elements.composables.GrayButton
import rs.etf.pmu.pocketsoccer.ui.elements.composables.StadiumBackground

@Composable
fun NewGameScreen(
    onSinglePlayerButtonClickCallback: () -> Unit,
    onMultiPlayerButtonClickCallback: () -> Unit
) {
    StadiumBackground {
        Column(
            modifier = Modifier
                .fillMaxHeight()
        ) {
            Row(
                modifier = Modifier
                    .weight(1 / 4f),
                verticalAlignment = Alignment.Bottom
            ) {
                Text(
                    text = "Play",
                    modifier = Modifier
                        .fillMaxWidth(),
                    style = MaterialTheme.typography.titleMedium,
                    textAlign = TextAlign.Center,
                )
            }

            Row(
                modifier = Modifier
                    .weight(3 / 4f)
                    .padding(start = 200.dp, end = 200.dp),
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxHeight(),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    GrayButton(
                        text = "Single player",
                        onClick = onSinglePlayerButtonClickCallback
                    )

                    Spacer(modifier = Modifier.height(30.dp))

                    GrayButton(
                        text = "Multiplayer",
                        onClick = onMultiPlayerButtonClickCallback
                    )
                }
            }
        }
    }
}
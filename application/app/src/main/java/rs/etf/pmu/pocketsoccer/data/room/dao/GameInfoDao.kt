package rs.etf.pmu.pocketsoccer.data.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import rs.etf.pmu.pocketsoccer.data.room.entities.GameInfo

@Dao
interface GameInfoDao {

    @Query(value = "SELECT * FROM game_info")
    suspend fun getAllGames(): List<GameInfo>

    @Query(value = "SELECT * FROM game_info WHERE id = :id")
    suspend fun getGameInfoById(id: Int): GameInfo

    @Insert(onConflict = OnConflictStrategy.ABORT)
    suspend fun insertGameInfo(gameInfo: GameInfo)

    @Query(value = "DELETE FROM game_info")
    suspend fun deleteAllGames()

    @Query(
        value = "SELECT * FROM game_info " +
                "WHERE (player_name_1 = :playerName1 AND player_name_2 = :playerName2) " +
                "OR (player_name_1 = :playerName2 AND player_name_2 = :playerName1)" +
                "ORDER BY id"
    )
    suspend fun getAllGamesForPlayers(playerName1: String, playerName2: String): List<GameInfo>

    @Query(
        value = "DELETE FROM game_info " +
                "WHERE (player_name_1 = :playerName1 AND player_name_2 = :playerName2) " +
                "OR (player_name_1 = :playerName2 AND player_name_2 = :playerName1)"
    )
    suspend fun deleteAllGamesForPlayers(playerName1: String, playerName2: String)
}
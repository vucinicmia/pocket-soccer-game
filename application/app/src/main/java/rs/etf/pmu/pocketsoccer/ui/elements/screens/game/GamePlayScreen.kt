package rs.etf.pmu.pocketsoccer.ui.elements.screens.game

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import rs.etf.pmu.pocketsoccer.game.settings.EndGameCondition
import rs.etf.pmu.pocketsoccer.ui.elements.composables.game.PauseButton

@Composable
fun GamePlayScreen(
    playerResult1: Int,
    playerResult2: Int,
    endGameCondition: EndGameCondition,
    timeElapsed: Long? = null,
    timeLimit: Long? = null,
    onPauseButtonClickCallback: () -> Unit = {},
) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(start = 20.dp, top = 10.dp)
    ) {
        Text(
            text = "$playerResult1 : $playerResult2",
            modifier = Modifier.align(Alignment.TopCenter),
            style = MaterialTheme.typography.displayLarge
        )

        if (endGameCondition == EndGameCondition.TIMEOUT) {
            timeLimit?.let { limit ->
                timeElapsed?.let {
                    Text(
                        text = "${it / 1000}:${limit / 1000}",
                        fontSize = 50.sp,
                        modifier = Modifier.align(Alignment.TopStart),
                        style = MaterialTheme.typography.displayLarge
                    )
                }
            }
        }

        PauseButton(
            modifier = Modifier
                .align(Alignment.TopEnd)
                .padding(20.dp)
                .size(50.dp),
            onClick = onPauseButtonClickCallback
        )
    }
}
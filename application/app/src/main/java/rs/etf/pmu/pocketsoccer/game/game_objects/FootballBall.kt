package rs.etf.pmu.pocketsoccer.game.game_objects

import rs.etf.pmu.pocketsoccer.game.utils.AccelerationVector
import rs.etf.pmu.pocketsoccer.game.utils.Coordinates
import rs.etf.pmu.pocketsoccer.game.utils.VelocityVector

class FootballBall(
    centerCoordinates: Coordinates,
    startCoordinates: Coordinates,
    r: Float,
    mass: Float,
    velocityVector: VelocityVector,
    accelerationVector: AccelerationVector
) : GameBall(centerCoordinates, startCoordinates, r, mass, velocityVector, accelerationVector)
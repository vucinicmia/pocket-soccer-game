package rs.etf.pmu.pocketsoccer.ui.navigation

interface Destination {
    val route: String
}

object OnboardScreenDestination : Destination {
    override val route: String
        get() = "onboard_screen"
}

object LogInScreenDestination : Destination {
    override val route: String
        get() = "log_in_screen"
}

object RegisterScreenDestination : Destination {
    override val route: String
        get() = "register_screen"
}

object StartScreenDestination : Destination {
    override val route: String
        get() = "start_screen"
}

object NewGameScreenDestination : Destination {
    override val route: String
        get() = "new_game_screen"
}

object SettingsScreenDestination : Destination {
    override val route: String
        get() = "settings_screen"
}

object FieldSettingsScreenDestination : Destination {
    override val route: String
        get() = "field_settings_screen"
}

object EndGameConditionSettingsScreenDestination : Destination {
    override val route: String
        get() = "end_game_condition_settings_screen"
}

object GameSpeedSettingsScreenDestination : Destination {
    override val route: String
        get() = "game_speed_settings_screen"
}

object StatsScreenDestination : Destination {
    override val route: String
        get() = "stats_screen"
}

object StatsDetailsScreenDestination : Destination {
    override val route: String
        get() = "stats_details_screen"
}

object EnterNamesScreenDestination : Destination {
    override val route: String
        get() = "enter_names_screen"
}

object ChoosePlayersScreenDestination : Destination {
    override val route: String
        get() = "choose_players_screen"
}
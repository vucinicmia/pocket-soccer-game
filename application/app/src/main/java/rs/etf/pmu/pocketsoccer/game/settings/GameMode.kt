package rs.etf.pmu.pocketsoccer.game.settings

enum class GameMode {
    MULTIPLAYER, SINGLE_PLAYER;

    companion object {

        const val INTENT_TAG = "game_mode"

        fun fromInt(value: Int) = when (value) {
            0 -> SINGLE_PLAYER
            1 -> MULTIPLAYER
            else -> throw IllegalArgumentException()
        }

        fun toInt(value: GameMode) = when (value) {
            SINGLE_PLAYER -> 0
            MULTIPLAYER -> 1
        }
    }
}
package rs.etf.pmu.pocketsoccer.game

enum class Player {
    PLAYER_1, PLAYER_2;

    companion object {

        const val INTENT_TAG_PLAYER_1_NAME = "player_1_name"
        const val INTENT_TAG_PLAYER_2_NAME = "player_2_name"

        const val PLAYER_1_DEFAULT_NAME = "Player 1"
        const val PLAYER_2_DEFAULT_NAME = "Player 2"

        const val INTENT_TAG_PLAYER_1_FLAG = "player_1_flag"
        const val INTENT_TAG_PLAYER_2_FLAG = "player_2_flag"

        fun fromInt(value: Int) = when (value) {
            0 -> PLAYER_1
            1 -> PLAYER_2
            else -> throw IllegalArgumentException()
        }

        fun toInt(value: Player) = when (value) {
            PLAYER_1 -> 0
            PLAYER_2 -> 1
        }
    }
}
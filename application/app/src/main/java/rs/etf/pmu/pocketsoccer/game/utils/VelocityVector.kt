package rs.etf.pmu.pocketsoccer.game.utils

data class VelocityVector(
    var vx: Float,
    var vy: Float
)
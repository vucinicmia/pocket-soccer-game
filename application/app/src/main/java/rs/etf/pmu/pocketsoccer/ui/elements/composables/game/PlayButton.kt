package rs.etf.pmu.pocketsoccer.ui.elements.composables.game

import androidx.compose.foundation.clickable
import androidx.compose.material3.Icon
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.PlayCircle
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@Composable
fun PlayButton(
    modifier: Modifier,
    onClick: () -> Unit
) {
    Icon(
        Icons.Outlined.PlayCircle,
        contentDescription = null,
        modifier = Modifier
            .then(modifier)
            .clickable(onClick = onClick)
    )
}
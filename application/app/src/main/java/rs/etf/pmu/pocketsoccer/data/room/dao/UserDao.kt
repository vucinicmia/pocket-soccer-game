package rs.etf.pmu.pocketsoccer.data.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import rs.etf.pmu.pocketsoccer.data.room.entities.User

@Dao
interface UserDao {

    @Query(value = "SELECT * FROM user WHERE username = :username")
    suspend fun getUser(username: String): User?

    @Insert(onConflict = OnConflictStrategy.ABORT)
    suspend fun insertUser(user: User)
}
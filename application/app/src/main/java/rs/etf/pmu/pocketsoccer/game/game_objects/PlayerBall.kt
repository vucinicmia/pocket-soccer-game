package rs.etf.pmu.pocketsoccer.game.game_objects

import rs.etf.pmu.pocketsoccer.game.Player
import rs.etf.pmu.pocketsoccer.game.utils.AccelerationVector
import rs.etf.pmu.pocketsoccer.game.utils.Coordinates
import rs.etf.pmu.pocketsoccer.game.utils.VelocityVector
import rs.etf.pmu.pocketsoccer.utils.WindowUtils

class PlayerBall(
    centerCoordinates: Coordinates,
    startCoordinates: Coordinates,
    r: Float,
    mass: Float,
    velocityVector: VelocityVector,
    accelerationVector: AccelerationVector,
    val player: Player,
    val flagImage: Int
) : GameBall(centerCoordinates, startCoordinates, r, mass, velocityVector, accelerationVector) {

    override fun toString(): String {
        return "Player() $id"
    }

    companion object {
        private val SCREEN_HEIGHT = WindowUtils.HEIGHT.toFloat()

        val PLAYER_RADIUS = SCREEN_HEIGHT / 10f
    }
}
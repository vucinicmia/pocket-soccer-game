package rs.etf.pmu.pocketsoccer.game.game_objects

import rs.etf.pmu.pocketsoccer.game.utils.Coordinates

data class Line(
    var startCoordinates: Coordinates,
    var endCoordinates: Coordinates,
    var lineRadius: Float = 0f
)
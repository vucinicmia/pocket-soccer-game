package rs.etf.pmu.pocketsoccer.repositories

import javax.inject.Inject
import javax.inject.Singleton
import rs.etf.pmu.pocketsoccer.data.room.entities.User
import rs.etf.pmu.pocketsoccer.data.room.dao.UserDao

@Singleton
class UserRepository @Inject constructor(private val userDao: UserDao) {

    suspend fun insertUser(user: User) {
        userDao.insertUser(user)
    }

    suspend fun getUser(username: String): User? {
        return userDao.getUser(username)
    }
}
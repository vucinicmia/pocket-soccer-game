package rs.etf.pmu.pocketsoccer.ui.theme

import androidx.compose.material3.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import rs.etf.pmu.pocketsoccer.R

val BrunoAceSCRegular = FontFamily(
    Font(R.font.bruno_ace_sc_regular),
    Font(R.font.bruno_ace_sc_regular, FontWeight.Bold)
)

// Set of Material typography styles to start with
val Typography = Typography(
    bodySmall = TextStyle(
        fontFamily = BrunoAceSCRegular,
        fontWeight = FontWeight.Normal,
        fontSize = 16.sp
    ),
    titleLarge = TextStyle(
        fontFamily = BrunoAceSCRegular,
        fontWeight = FontWeight.Bold,
        fontSize = 80.sp
    ),
    titleMedium = TextStyle(
        fontFamily = BrunoAceSCRegular,
        fontWeight = FontWeight.Bold,
        fontSize = 60.sp
    ),
    titleSmall = TextStyle(
        fontFamily = BrunoAceSCRegular,
        fontWeight = FontWeight.Bold,
        fontSize = 40.sp
    ),
    displayLarge = TextStyle(
        fontFamily = BrunoAceSCRegular,
        fontWeight = FontWeight.Bold,
        fontSize = 40.sp
    ),
    displayMedium = TextStyle(
        fontFamily = BrunoAceSCRegular,
        fontWeight = FontWeight.Bold,
        fontSize = 30.sp
    ),
    displaySmall = TextStyle(
        fontFamily = BrunoAceSCRegular,
        fontWeight = FontWeight.Bold,
        fontSize = 20.sp
    )

    /* Other default text styles to override
    titleLarge = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        fontSize = 22.sp,
        lineHeight = 28.sp,
        letterSpacing = 0.sp
    ),
    labelSmall = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Medium,
        fontSize = 11.sp,
        lineHeight = 16.sp,
        letterSpacing = 0.5.sp
    )
    */
)
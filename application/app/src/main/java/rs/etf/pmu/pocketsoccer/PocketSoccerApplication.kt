package rs.etf.pmu.pocketsoccer

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class PocketSoccerApplication : Application()
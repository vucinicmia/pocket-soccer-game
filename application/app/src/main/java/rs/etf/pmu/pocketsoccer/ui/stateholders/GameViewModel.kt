package rs.etf.pmu.pocketsoccer.ui.stateholders

import android.app.Application
import android.media.MediaPlayer
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import rs.etf.pmu.pocketsoccer.R
import rs.etf.pmu.pocketsoccer.game.Game
import rs.etf.pmu.pocketsoccer.game.GameState
import rs.etf.pmu.pocketsoccer.game.GameStates
import rs.etf.pmu.pocketsoccer.game.Player
import rs.etf.pmu.pocketsoccer.game.game_objects.Line
import rs.etf.pmu.pocketsoccer.game.settings.EndGameCondition
import rs.etf.pmu.pocketsoccer.game.settings.GameMode
import rs.etf.pmu.pocketsoccer.game.settings.GameSpeed
import rs.etf.pmu.pocketsoccer.game.utils.Coordinates

data class UiState(
    val game: Game,
    val change: Boolean = false,
    val draggingLine: Line = Line(
        startCoordinates = Coordinates(0f, 0f),
        endCoordinates = Coordinates(0f, 0f),
        lineRadius = 10f
    ),
    val dragging: Boolean = false,
)

class GameViewModel(application: Application) : AndroidViewModel(application) {
    private val _uiState = MutableStateFlow(UiState(Game))
    val uiState = _uiState.asStateFlow()

    private var job: Job? = null

    companion object {
        private const val MAX_TIME_TO_MOVE = 5000L
        private const val TIME_TO_MOVE_DELAY = 2000L
    }

    fun cancelJob() {
        job?.cancel()
        job = null
    }

    fun start(
        gameMode: GameMode,
        playerName1: String,
        playerName2: String,
        playerFlag1: Int,
        playerFlag2: Int,
        endGameCondition: EndGameCondition,
        numberOfGoals: Int,
        timeout: Long,
        fieldImageId: Int,
        gameSpeed: GameSpeed
    ) {
        _uiState.value.game.initGame(
            gameMode = gameMode,
            playerName1 = playerName1,
            playerName2 = playerName2,
            playerFlag1 = playerFlag1,
            playerFlag2 = playerFlag2,
            endGameCondition = endGameCondition,
            numberOfGoals = numberOfGoals,
            timeout = timeout,
            fieldImageId = fieldImageId,
            gameSpeed = gameSpeed
        )
    }

    fun restoreGame(gameState: GameState) {
        gameState.changeGameState(GameStates.PAUSED)
        gameState.gameBalls.removeAll { true }
        gameState.teamPlayers1.forEach {
            gameState.gameBalls.add(it)
        }
        gameState.teamPlayers2.forEach {
            gameState.gameBalls.add(it)
        }
        gameState.gameBalls.add(gameState.ball)

        _uiState.value.game.gameState = gameState

        gameLoop()
    }

    fun gameLoop() {
        job = CoroutineScope(Dispatchers.Default).launch {
            var tp1 = System.currentTimeMillis()
            var tp2: Long

            val gameState = _uiState.value.game.gameState

            gameState.moveTime = 0

            while (true) {
                tp2 = System.currentTimeMillis()
                val elapsedTime = (tp2 - tp1).toFloat() / 1000
                val moveTime = tp2 - tp1
                tp1 = tp2

                synchronized(Game) {

                    if (gameState.currentGameState == GameStates.RUNNING) {

                        gameState.timeElapsed += moveTime

                        if (gameState.gameMode == GameMode.MULTIPLAYER) {
                            if (Game.selectedPlayer == null) {
                                gameState.moveTime += moveTime
                            }
                        } else {
                            if (gameState.playerOnTurn == Player.PLAYER_1) {
                                if (gameState.moveTime in TIME_TO_MOVE_DELAY..MAX_TIME_TO_MOVE) {
                                    gameState.findAndMoveClosestPlayerToTheBall()
                                    gameState.switchTurn()
                                } else {
                                    gameState.moveTime += moveTime
                                }
                            } else {
                                if (Game.selectedPlayer == null) {
                                    gameState.moveTime += moveTime
                                }
                            }
                        }

                        if (gameState.moveTime > MAX_TIME_TO_MOVE) {
                            gameState.switchTurn()
                        }

                        if (gameState.endGameCondition == EndGameCondition.TIMEOUT &&
                            gameState.timeElapsed >= gameState.timeLimit
                        ) {
                            gameState.changeGameState(GameStates.FINISHED)
                        }

                        _uiState.value.game.onUpdate(
                            elapsedTime * GameSpeed.toFloat(gameState.gameSpeed),
                            onGoalScoredCallback = {
                                playSound(R.raw.crowd)
                            }, onBallCollisionCallback = {
                                playSound(R.raw.bounce)
                            }
                        )

                        val selectedPlayer = _uiState.value.game.selectedPlayer
                        selectedPlayer?.let { player ->
                            val draggingLine = _uiState.value.draggingLine

                            _uiState.update {
                                it.copy(
                                    draggingLine = Line(
                                        startCoordinates = player.center,
                                        endCoordinates = draggingLine.endCoordinates,
                                        lineRadius = draggingLine.lineRadius
                                    )
                                )
                            }
                        }
                    } else if (gameState.currentGameState == GameStates.FINISHED) {
                        job?.cancel()
                    } else {
                        return@synchronized
                    }
                }

                _uiState.update { it.copy(change = !_uiState.value.change) }
                delay(1L)
            }
        }
    }

    private fun playSound(soundId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            MediaPlayer.create(getApplication(), soundId).start()
        }
    }

    fun setSelectedPlayer(x: Float, y: Float) {
        val gameState = _uiState.value.game.gameState
        synchronized(Game) {
            val shouldDrag = gameState.gameMode == GameMode.MULTIPLAYER ||
                    (gameState.gameMode == GameMode.SINGLE_PLAYER &&
                            gameState.playerOnTurn != Player.PLAYER_1)

            if (shouldDrag) {
                Game.setSelectedPlayer(x, y)
                _uiState.update { it.copy(change = !_uiState.value.change) }
            }
        }
    }

    fun unselectPlayer() {
        synchronized(Game) {
            Game.unselectPlayer()
            _uiState.update { it.copy(change = !_uiState.value.change) }
        }
    }

    fun startDrag() {
        val gameState = _uiState.value.game.gameState
        synchronized(Game) {
            val shouldDrag = gameState.gameMode == GameMode.MULTIPLAYER ||
                    (gameState.gameMode == GameMode.SINGLE_PLAYER &&
                            gameState.playerOnTurn != Player.PLAYER_1)

            if (shouldDrag && Game.selectedPlayer != null) {
                val selectedPlayer = Game.selectedPlayer!!
                val draggingLine = _uiState.value.draggingLine

                _uiState.update {
                    it.copy(
                        draggingLine = Line(
                            startCoordinates = selectedPlayer.center,
                            endCoordinates = selectedPlayer.center,
                            lineRadius = draggingLine.lineRadius
                        ),
                        dragging = true
                    )
                }
            }
        }
    }

    fun onDrag(offsetX: Float, offsetY: Float) {
        val gameState = _uiState.value.game.gameState
        synchronized(Game) {
            val shouldDrag = gameState.gameMode == GameMode.MULTIPLAYER ||
                    (gameState.gameMode == GameMode.SINGLE_PLAYER &&
                            gameState.playerOnTurn != Player.PLAYER_1)

            if (shouldDrag && Game.selectedPlayer != null
            ) {
                val draggingLine = _uiState.value.draggingLine

                with(draggingLine) {
                    _uiState.update {
                        it.copy(
                            draggingLine = Line(
                                startCoordinates = startCoordinates,
                                endCoordinates = endCoordinates.copy(
                                    x = endCoordinates.x + offsetX,
                                    y = endCoordinates.y + offsetY
                                ),
                                lineRadius = lineRadius
                            )
                        )
                    }
                }
            }
        }
    }

    fun onEndDrag() {
        val gameState = _uiState.value.game.gameState
        synchronized(Game) {
            val shouldDrag = gameState.gameMode == GameMode.MULTIPLAYER ||
                    (gameState.gameMode == GameMode.SINGLE_PLAYER &&
                            gameState.playerOnTurn != Player.PLAYER_1)

            if (shouldDrag && Game.selectedPlayer != null) {
                val selectedPlayer = Game.selectedPlayer!!
                val draggingLine = _uiState.value.draggingLine

                // Giving player velocity proportional to the length of dragging line
                selectedPlayer.velocityVector.vx =
                    3f * (selectedPlayer.center.x - draggingLine.endCoordinates.x)
                selectedPlayer.velocityVector.vy =
                    3f * (selectedPlayer.center.y - draggingLine.endCoordinates.y)

                _uiState.update {
                    it.copy(
                        dragging = false,
                        draggingLine = Line(
                            startCoordinates = Coordinates(0f, 0f),
                            endCoordinates = Coordinates(0f, 0f),
                            lineRadius = _uiState.value.draggingLine.lineRadius
                        )
                    )
                }
                gameState.switchTurn()
                unselectPlayer()
                if (job == null) gameLoop()
            }
        }
    }

    fun pauseGame() {
        val gameState = _uiState.value.game.gameState
        synchronized(Game) {
            if (gameState.currentGameState == GameStates.RUNNING) {
                gameState.changeGameState(GameStates.PAUSED)

                _uiState.update { it.copy(change = !_uiState.value.change) }
            }
        }
    }

    fun runGame() {
        val gameState = _uiState.value.game.gameState
        synchronized(Game) {
            if (gameState.currentGameState == GameStates.NOT_STARTED) {
                if (job == null) {
                    gameLoop()
                }
            }

            gameState.changeGameState(GameStates.RUNNING)

            _uiState.update { it.copy(change = !_uiState.value.change) }
        }
    }
}
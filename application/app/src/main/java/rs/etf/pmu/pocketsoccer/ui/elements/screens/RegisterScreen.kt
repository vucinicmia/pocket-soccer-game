package rs.etf.pmu.pocketsoccer.ui.elements.screens

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import rs.etf.pmu.pocketsoccer.ui.elements.composables.GrayButton
import rs.etf.pmu.pocketsoccer.ui.elements.composables.InputTextField
import rs.etf.pmu.pocketsoccer.ui.elements.composables.StadiumBackground
import rs.etf.pmu.pocketsoccer.ui.stateholders.StartActivityViewModel

@Composable
fun RegisterScreen(
    viewModel: StartActivityViewModel,
    takePhotoCallback: () -> Unit,
    onSuccessfulRegistrationCallback: () -> Unit,
    onErrorCallback: (String) -> Unit
) {
    val uiState by viewModel.uiStateRegisterScreen.collectAsState()

    StadiumBackground {
        Text(
            text = "Register",
            modifier = Modifier
                .fillMaxWidth(),
            style = MaterialTheme.typography.titleLarge,
            textAlign = TextAlign.Center,
        )

        Spacer(modifier = Modifier.height(20.dp))

        InputTextField(
            modifier = Modifier.fillMaxWidth(),
            label = "Username",
            text = uiState.username,
            onValueChange = viewModel::updateUsernameRegisterScreen,
            imeAction = ImeAction.Next
        )

        Spacer(modifier = Modifier.height(20.dp))

        InputTextField(
            modifier = Modifier.fillMaxWidth(),
            label = "Password",
            text = uiState.password,
            onValueChange = viewModel::updatePasswordRegisterScreen,
            imeAction = ImeAction.Done,
            password = true
        )

        Spacer(modifier = Modifier.height(20.dp))

        Row(
            modifier = Modifier
                .fillMaxSize()
                .padding(start = 100.dp, end = 100.dp),
            horizontalArrangement = Arrangement.Center,
        ) {
            GrayButton(
                text = "Take photo",
                onClick = { viewModel.takePhoto(takePhotoCallback, onErrorCallback) },
                modifier = Modifier.weight(1f),
                backgroundColor = if (uiState.photoTaken) Color.Gray else Color.DarkGray
            )

            Spacer(modifier = Modifier.width(50.dp))

            if (uiState.photoTaken) {
                GrayButton(
                    text = "Register",
                    onClick = {
                        viewModel.register(
                            onSuccessfulRegistrationCallback,
                            onErrorCallback
                        )
                    },
                    modifier = Modifier.weight(1f)
                )
            } else {
                Spacer(modifier = Modifier.weight(1f))
            }
        }
    }
}
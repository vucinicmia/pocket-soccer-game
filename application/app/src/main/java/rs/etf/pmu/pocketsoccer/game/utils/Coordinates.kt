package rs.etf.pmu.pocketsoccer.game.utils

import androidx.compose.ui.geometry.Offset
import kotlin.math.pow
import kotlin.math.sqrt

data class Coordinates(
    var x: Float,
    var y: Float
) {
    fun toOffset(): Offset {
        return Offset(x, y)
    }

    fun distanceTo(other: Coordinates): Float {
        return sqrt((x - other.x).toDouble().pow(2.0) + (y - other.y).toDouble().pow(2.0)).toFloat()
    }
}

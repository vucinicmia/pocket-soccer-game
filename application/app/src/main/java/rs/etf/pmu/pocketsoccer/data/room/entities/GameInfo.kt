package rs.etf.pmu.pocketsoccer.data.room.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.Date

@Entity(tableName = "game_info")
data class GameInfo constructor(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    @ColumnInfo(name = "player_name_1") val playerName1: String,
    @ColumnInfo(name = "player_name_2") val playerName2: String,
    @ColumnInfo(name = "player_score_1") val playerScore1: Int,
    @ColumnInfo(name = "player_score_2") val playerScore2: Int,
    val date: Date
)
package rs.etf.pmu.pocketsoccer.ui.elements.screens

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import rs.etf.pmu.pocketsoccer.ui.elements.composables.GrayButton
import rs.etf.pmu.pocketsoccer.ui.elements.composables.InputTextField
import rs.etf.pmu.pocketsoccer.ui.elements.composables.StadiumBackground
import rs.etf.pmu.pocketsoccer.ui.stateholders.StartGameActivityViewModel

@Composable
fun EnterNamesScreen(
    viewModel: StartGameActivityViewModel,
    onContinueButtonClickCallback: () -> Unit
) {

    val uiState by viewModel.uiStateEnterNamesScreen.collectAsState()

    StadiumBackground {
        Column(
            modifier = Modifier
                .fillMaxSize()
        ) {
            Row(
                modifier = Modifier
                    .weight(1 / 4f)
                    .fillMaxSize(),
                verticalAlignment = Alignment.Bottom
            ) {
                Text(
                    text = "Enter names",
                    modifier = Modifier
                        .fillMaxWidth(),
                    style = MaterialTheme.typography.titleMedium,
                    textAlign = TextAlign.Center,
                )
            }

            Column(
                modifier = Modifier
                    .weight(3 / 4f)
                    .fillMaxHeight()
                    .padding(start = 100.dp, end = 100.dp),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                InputTextField(
                    label = "Player 1",
                    text = uiState.playerName1,
                    onValueChange = viewModel::updatePlayerName1,
                    imeAction = ImeAction.Next
                )

                Spacer(modifier = Modifier.height(30.dp))

                InputTextField(
                    label = "Player 2",
                    text = uiState.playerName2,
                    onValueChange = viewModel::updatePlayerName2,
                    imeAction = ImeAction.Done
                )

                Spacer(modifier = Modifier.height(30.dp))

                GrayButton(
                    text = "Continue",
                    onClick = onContinueButtonClickCallback
                )
            }
        }
    }
}
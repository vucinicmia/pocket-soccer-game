package rs.etf.pmu.pocketsoccer.game.utils

data class AccelerationVector(
    var ax: Float,
    var ay: Float
)
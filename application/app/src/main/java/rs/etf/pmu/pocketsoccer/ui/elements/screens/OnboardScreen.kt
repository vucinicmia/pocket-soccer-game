package rs.etf.pmu.pocketsoccer.ui.elements.screens

import androidx.compose.foundation.layout.*
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import rs.etf.pmu.pocketsoccer.ui.elements.composables.GrayButton
import rs.etf.pmu.pocketsoccer.ui.elements.composables.StadiumBackground

@Composable
fun OnboardScreen(
    onButtonLogInClickedCallback: () -> Unit,
    onButtonRegisterClickedCallback: () -> Unit
) {
    StadiumBackground {
        Text(
            text = "Pocket\nSoccer",
            modifier = Modifier
                .fillMaxWidth(),
            style = MaterialTheme.typography.titleLarge,
            textAlign = TextAlign.Center,
        )

        Spacer(modifier = Modifier.height(30.dp))

        Row(
            modifier = Modifier
                .fillMaxSize()
                .padding(start = 100.dp, end = 100.dp),
            horizontalArrangement = Arrangement.Center
        ) {
            GrayButton(
                modifier = Modifier.weight(1f),
                text = "Log In",
                onClick = onButtonLogInClickedCallback
            )
            Spacer(modifier = Modifier.width(50.dp))
            GrayButton(
                modifier = Modifier.weight(1f),
                text = "Register",
                onClick = onButtonRegisterClickedCallback
            )
        }
    }
}
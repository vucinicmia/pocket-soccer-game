# Pocket Soccer game
- An Android game inspired by Pocket Soccer game, made using Jetpack Compose

## Preview

#### Start screen

![Alt text](screenshots/start-screen.jpg?raw=true)

#### Play screen

![Alt text](screenshots/play-screen.jpg?raw=true)

#### Pause screen

![Alt text](screenshots/pause-screen.jpg?raw=true)

#### Goal screen

![Alt text](screenshots/goal-screen.jpg?raw=true)

## Demo

https://youtu.be/l_fupGoYnuY

## Image sources
- Field images, ball and sounds - https://github.com/petalex/pocketsoccer/tree/master/PocketSoccer/app/src/main/assets
- Flags - https://flagpedia.net